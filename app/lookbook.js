'use strict';

import { runAsync } from './molecules/helpers';
import * as MoleculesList from './molecules';
import * as ComponentsList from './components/index.js';
import * as Common from './common/scripts/common.js';
import { YoutubePlayer } from './components/video/youtube-player/youtube-player';
import { BrightcovePlayer } from './components/video/brightcove-player/brightcove-player';


//Initialize common functionality
window.GLOBALS = {};
for (let commonItem in Common) {
  GLOBALS[commonItem] = (!Common[commonItem].isConstructor) ? new Common[commonItem]() : Common[commonItem];
}
window.GLOBALS.YoutubePlayer = YoutubePlayer;
window.GLOBALS.BrightcovePlayer = BrightcovePlayer;


$(() => {
  // Initialize all molecules
  for (let moleculeName in MoleculesList) {
    ((Molecule) => {
      $(Molecule.selector).each((_, $el) => {
        new Molecule($el);
      });
    })(MoleculesList[moleculeName]);
  }

  $(window).trigger('modules.initialized');

  // Initialize all components
  for (let componentName in ComponentsList) {
    ((Component) => {
      if (Component.selector) {
        $(Component.selector).each((_, $el) => runAsync(() => {
          new Component($el);
        }));
      } else {
        new Component();
      }
      
    })(ComponentsList[componentName]);
  }
});