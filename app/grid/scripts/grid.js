export class Grid {

  get selectors() { 
    return {
      equalCells: '.equal-cells',
      collageAppearance: '.collage-layout'
    }
  }

  constructor(...args) {
    this._init();
  }

  _init() {
    $(this.selectors.equalCells).each((index, el) => {
      let cells = $(el).find('> .row > [class*="span"]');
      let wrapper = $(el);
      this._updateEqualCells(cells, wrapper);
      this._correctVerticalSpacings(cells);
      $(window).on('resize load', _.throttle(() => this._updateEqualCells(cells, wrapper), 50));
      $(el).on('update.equal.cells', () => this._updateEqualCells(cells, wrapper));
    });

    $(this.selectors.collageAppearance).each((index, el) => {
      let sliderEl = $(el).find('> .row'),
        cells = sliderEl.find('> [class*="span"]'),
        wideCell = cells.filter((index, el) => $(el).hasClass('span16'));
      
      wideCell.length && $(el).addClass('wide-cell-position-' + wideCell.index());
      cells.each((index, el) => $(el).attr('data-index', $(el).index()));
      sliderEl.on('destroy', () => {
        cells
          .sort((a, b) => +$(a).attr('data-index') - +$(b).attr('data-index'))
          .appendTo(sliderEl);
      });
    });
  }

  _updateEqualCells(cells, wrapper) {
    //for sliders cells are cloned, so we need to extend a set of cells to equal height
    let isAdaptiveSlider = cells.parent('.slick-track').length,
        groups;
    
    cells = isAdaptiveSlider ? cells.add(cells.parent().find('>.slick-cloned')) : cells;
    groups = _.groupBy(cells, (el) => $(el).offset().top);

    if (cells.eq(0).closest('.align-cta-bottom').length) {
      for (let i in groups) {
         handleAlignCTAs($(groups[i]));
      }
    }

    if (Object.keys(groups).length != cells.length || isAdaptiveSlider) {
      for (let i in groups) {
        calculateHeight($(groups[i]));
      }
    } else {
      cells.height('auto');
      onAutoHeight(cells);
    }
    

    function calculateHeight(cells) {
      let max = 0;
      cells.height('auto');
      onBeforeCellHeightUpdate(cells);
      max = cells.eq(0).height();
      for (let i = 1; i < cells.length; i++) {
        max = cells.eq(i).height() > max ? cells.eq(i).height() : max;
      }
      cells.height(max);
    }

    function handleAlignCTAs(cells) {
      let max = 0,
          ctasAmount = 0;
      cells.each((i, el) => {
        ctasAmount = $(el).find('.molecule-lb-501 .cta-wrapper > *:not(br)').length;
        max = ctasAmount > max ? ctasAmount : max;
      });
      cells.removeClass('ctas-amount-1 ctas-amount-2 ctas-amount-3').addClass(`ctas-amount-${max}`);
    }

    /**
     * @Description helper function to do some updates before CELL's height is updated. Is used for 
     *              recalculation of some inner heights like for align-description etc.
     */
    function onBeforeCellHeightUpdate(cells) {
      cells.first().find('.molecule-lb-501').first().trigger('equalheight', {cells: cells});
    }

    function onAutoHeight(cells) {
      cells.first().find('.molecule-lb-501').first().trigger('equalheightauto', {cells: cells});
    }
  }

  _correctVerticalSpacings(cells) {
    let classNamesToCopy = ['mtop-mol', 'mbot-mol', 'ptop', 'pbot'];
    cells.each((index, el) => {
      var childMolecule = $(el).find('>[class*="molecule"]');
      copyClassNames($(el), childMolecule);
      childMolecule.removeClass(classNamesToCopy.join(' ')).addClass('equal-cells-initialized');
    });
    function copyClassNames(cell, mol) {
      classNamesToCopy.forEach((name) => {
        mol.hasClass(name) && cell.addClass(name) && mol.removeClass(name)
      });
    }
  }

}

