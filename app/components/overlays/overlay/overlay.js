import { BaseComponent } from '../../base-component.js';


export class Overlay extends BaseComponent {

  static get OPEN_EVENT() { return 'overlay-opened'; }
  static get CLOSE_EVENT() { return 'overlay-closed'; }

  get defaults() {
    return $.extend({}, super.defaults, {
      closeButtonSelector: '.js_pop_close',
      overlayWrapperClass: 'overlay-wrapper body',
      openedClass: 'opened',
      noScrollClass: 'no-scroll'
    });
  }

  constructor(...args) {
    super(...args);

    this.closeButton = this.el.find(this.options.closeButtonSelector);
    this.openers = $('a[rel="' + this.el.attr('id') + '"]');

    //Append overlay to body only in case if it is unique, otherwise - just remove it CAP-3485
    if (!$('.overlay-wrapper.body #' + this.el.attr('id')).length) {
      this.el.wrap('<div class="' + this.options.overlayWrapperClass + '"></div>').parent().appendTo(document.body);
    } else {
      this.el.remove();
    }
    this._initListeners();
    this._innerModules = this.el.find('[class*="flex2-molecule"]');
  }

  _initListeners() {
    this.openers.on('click', (e) => this.open(e));
    this.closeButton.on('click', () => this.close());
    this.el.parent().on('click', (e) => {
      $(e.target).hasClass('overlay-wrapper') && this.close();
    });
  }

  open(e) {
    this._innerModules.trigger('overlay.beforeopen');
    this._onOpen();
    this._innerModules.trigger('overlay.opened');

    //one time listen for ESC pressed to close overlay
    $(document).one('keyup.overlay', (e) => {
      (e.keyCode == 27) && this.close();
    });
    e.preventDefault();
  }

  _onOpen() {
    this._preventScrolling();

    this.el.addClass(this.options.openedClass);
    this.el.parent().addClass(this.options.openedClass);
  }

  close() {
    $(document).off('keyup.overlay');
    this._onClose();
  }

  _onClose() {
    this.el.removeClass(this.options.openedClass);
    this.el.parent().removeClass(this.options.openedClass);
    this._innerModules.trigger('overlay.closed');
    this._releaseScrolling();
  }

  _preventScrolling() {
    $(document.body).addClass(this.options.noScrollClass);
  }

  _releaseScrolling() {
    $(document.body).removeClass(this.options.noScrollClass);
  }
}
