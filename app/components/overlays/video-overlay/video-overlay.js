import { Overlay } from '../overlay/overlay.js';
import { YoutubePlayer } from '../../video/youtube-player/youtube-player.js';
import { BrightcovePlayer } from '../../video/brightcove-player/brightcove-player.js';


export class VideoOverlay extends Overlay {
  // This class is added automatically to this.el component
  get ROOT_CLASS() { return 'video-overlay'; }

  get defaults() {
    return $.extend({}, super.defaults, {
      youtubeVideoSelector: '.youtube-video-template',
      brightcoveVideoSelector: '.bc-video-template',
      videoContainerSelector: '.video-container'
    });
  }

  constructor(...args) {
    super(...args);
    this._spinner = this._createVideoSpinner();
  }

  _createVideoPlayer() {
    let youtubeVideoElement = this.el.find(this.options.youtubeVideoSelector);
    let brightcoveVideoElement = this.el.find(this.options.brightcoveVideoSelector);
    if (youtubeVideoElement.length) {
      return new YoutubePlayer(youtubeVideoElement);
    } else if (brightcoveVideoElement.length) {
      return new BrightcovePlayer(brightcoveVideoElement);
    }

    console.warn('No supported video element in VideoOverlay');
    return {};
  }

  _createVideoSpinner() {
    if (!this._videoSpinner) {
      this.spinner = $('<div/>', {'class': 'spinner'});
      this.el.find(this.options.videoContainerSelector).append(this.spinner);
    }
  }

  _onOpen() {
    if (!this._videoPlayer) {
      this._videoPlayer = this._createVideoPlayer();
      this._videoPlayer.init(() => this._hideSpinner());
      //if player was not initialized - don't autostart video on mobiles
      if (GLOBALS.Utils.device.isMobile) {
        super._onOpen();
        return false;
      }
    }
    this._startVideo();
    super._onOpen();
  }

  _onClose() {
    this._pauseVideo();
    super._onClose();
  }

  _startVideo() {
    this._videoPlayer.play();
  }

  _pauseVideo() {
    this._videoPlayer.pause();
  }

  _hideSpinner() {
    this.spinner.fadeOut();
  }
}
