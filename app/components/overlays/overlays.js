'use strict';

import { BaseComponent } from '../base-component.js';
import { Overlay } from './overlay/overlay.js';
import { VideoOverlay } from './video-overlay/video-overlay.js';

export class OverlaysFactory {
  
  static get selector() { return '.overlay-popup, .content-overlay'; }
  get options() {
    return {
      closeOverlayIconTpl: ''+
        '<a href="javascript:void(0);" class="js_pop_close popup-close" title="close button">'+
          '<span class="screenReading">Close</span>'+
        '</a>'
    }
  }
  get overlayTypes() {
    return {
      videoOverlaySelector: '.video-container',
      contentOverlayClass: 'content-overlay'
    }
  }
  constructor(el) {
    this._init(el);
  }

  _init(el) {
    this.el = $(el);
    //Initiate video Overlay
    if (this.el.find(this.overlayTypes.videoOverlaySelector).length && !this.el.hasClass(this.overlayTypes.contentOverlayClass)) {
      new VideoOverlay(el);
      return;
    }
    //Initiate content Overlay
    if (this.el.hasClass(this.overlayTypes.contentOverlayClass)) {
      this._prepareContent() && new Overlay(this.el[0]);
      return;
    }

    //Initiate regular Overlay, assuming that it is not a special one
    new Overlay(el);
  }

  _prepareContent() {
    this._removeInlineScripts();
    let popupId = this.el.find('[data-content-overlay-id]').attr('data-content-overlay-id'),
        openers = $('a[href="#' + popupId + '"]');
    if (popupId && openers.length) {
      this.el = this.el.removeClass(this.overlayTypes.contentOverlayClass)
              .wrap('<div class="overlay-popup ' + this.overlayTypes.contentOverlayClass + '"></div>')
              .parent()
              .attr('id', popupId)
              .append(this.options.closeOverlayIconTpl);
      openers.attr('rel', popupId);
    }
    return this.el;
  }

  //remove inline scripts within overlay's content to prevent double evaluation
  _removeInlineScripts() {
    this.el.find('script').remove();
  }

}