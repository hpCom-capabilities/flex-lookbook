/**
 * @constructor LbSlider
 * @param {jquery DOM el} el - slider root element
 * @param {object} options - slider configuration. Supports
                  {number} visibleSlides - how many slides are visible on desktop. Might have decimals, eg 2.5
                  {function} onSlide  - callback to fire once sliding started. Index of the slide is passed as
                                        parameter
                  {string} customClass - any additional classname to add to a slider
                  {string} alignArrowsToEl - selector, to align arrows vertically to
                  {number} responsiveBp - breakpoint for when should slider apply responsive settings
                                          default - 0, meaning no responsive transformations at all
                  {number} responsiveVisibleSlides - how many slides are visible on mobiles. Might have decimals

 * @description - creates Google-play look like slider. On mobiles swipe makes slider to scroll to
                  a certain slide, respecting swipe power and properly aligning target slide to the
                  left edge of the slider. Bouncing effect when swiping out of working area
                  is added by default.
 */
export class LbSlider {

  get defaults() {
    return {
      sliderClass: 'lb-slider animate',
      noArrowsClass: 'no-arrows',
      currentSlide: 0,
      visibleSlides: 1,
      infinity: true,
      customClass: '',
      alignArrowsToEl: null,
      hideArrowsWhenCantSlide: true,
      responsive: [],
      rtl: GLOBALS.Utils.isRTL(),
      onSlide: () => true,
      waitForTrue: () => true
    }
  }
  constructor(el, options) {
    this.o = $.extend({}, this.defaults, options);
    this.o.RTL = this.o.rtl ? -1 : 1;
    this._init(el);
  }

  _init(el) {
    this._bootstrap(el);
    this._initResponsiveSettings();
    this._refresh();
    this._activateSlide(this.o.currentSlide);
    this._initListeners();
  }

  _bootstrap(el) {
    this.s = {
      el: el,
      slides: $(el).children(),
      currentSlide: this.o.currentSlide,
      nextIndex: 0,
      slidesCount: el.children().length,
      slideWidth: 0,
      trackWidth: 0,
      infinity: this.o.infinity,
      visibleSlides: this.o.visibleSlides,
      responsive: this.o.responsive
    }
    this.s.el.addClass(this.o.sliderClass + ' ' + this.o.customClass);
    this._addLbSliderTrack();
    this._addControls();
  }

  _initResponsiveSettings() {
    if (this.s.responsive.length) {

      //sort by bp (from the biggest to smallest)
      this.s.responsive = _.sortBy(this.s.responsive, 'bp');

      //clone desktopSettings
      this.s.desktop = {
        visibleSlides: this.s.visibleSlides
      }

    }
  }

  _checkResponsiveSettings() {
    if (this.s.responsive.length) {
      let applySettings;
      for (let i = 0; i < this.s.responsive.length; i++) {
        if (window.matchMedia(`screen and (max-width: ${this.s.responsive[i].bp}px)`).matches) {
          applySettings = this.s.responsive[i];
          break;
        }
      }
      applySettings = applySettings || this.s.desktop;
      this._applySettings(applySettings);
    }
  }

  _applySettings(updateSettings) {
    $.extend(this.s, updateSettings);
  }

  _addLbSliderTrack() {
    this.s.list = $('<div />', {class: 'lb-slider-list'});
    this.s.track = $('<div />', {class: 'lb-slider-track'});
    this.s.list.append(this.s.track.append(this.s.slides));
    this.s.el.append(this.s.list.append(this.s.track));
  }

  _getTrackWidth() {
    return this.s.slideWidth * this.s.slidesCount;
  }

  _getSlideWidth() {
    if (!this.s.visibleSlides) {
      let regexpr = new RegExp(/span(\d*)/ig),
          slideClass = this.s.slides.first().attr('class'),
          test = regexpr.exec(slideClass);
      
      this.s.visibleSlides = (test && test[1]) ? 24 / (+test[1]) : 1
    }
    
    return this.s.el.width() / this.s.visibleSlides;
  }

  _addControls() {
    this.s.nextArrow = $('<button />', {class: 'next', text: 'Next'});
    this.s.prevArrow = $('<button />', {class: 'prev', text: 'Prev'});
    this.s.el.append(this.s.prevArrow).append(this.s.nextArrow);

    //align arrows to some element within slider (e.g. to the middle of the img)
    this.o.alignArrowsToEl && GLOBALS.SliderHelper.alignArrowsToEl(this.s.el, {
      arrows: this.s.nextArrow.add(this.s.prevArrow),
      alignToEl: this.s.slides.first().find(this.o.alignArrowsToEl)
    });
  }

  _refresh() {
    this._checkResponsiveSettings();
    this._updateModel();
    this._updateDOM();
  }

  _updateModel() {
    this.s.slideWidth = this._getSlideWidth();
    this.s.trackWidth = this._getTrackWidth();
    this.s.maxTranslate = this.s.trackWidth - this.s.el.width();
    this.s.maxTranslate = (this.s.maxTranslate < 0) ? 0 : this.s.maxTranslate;
  }

  _updateDOM() {
    this.s.slides.width(this.s.slideWidth);
    this.s.track.width(this.s.trackWidth);
    this.slideTo(this.s.currentSlide);

    //hide arrows when no content to scroll
    this._updateArrows();

    this.s.el.trigger('setPosition');
  }

  _updateArrows() {
    if (this.o.hideArrowsWhenCantSlide) {
      this.s.el[this.s.el.width() >= this.s.trackWidth ? 'addClass' : 'removeClass'](this.o.noArrowsClass);
    }
  }

  _onSlideChange(index) {
    let lastSlideIndex = this.s.slides.length - 1;

    if (!this.s.infinity) {
      (index === 0) && this.s.prevArrow.addClass('disabled') && this.s.nextArrow.removeClass('disabled');
      (index > 0 && index < lastSlideIndex) && this.s.prevArrow.removeClass('disabled') && this.s.nextArrow.removeClass('disabled');
      (index === lastSlideIndex) && this.s.prevArrow.removeClass('disabled') && this.s.nextArrow.addClass('disabled');
    }

  }

  _initListeners() {
    $(window).on('resize orientationchange load', _.debounce(() => this._refresh(), 50));
    this.s.nextArrow .on('click', () => this.slideNext());
    this.s.prevArrow .on('click', () => this.slidePrev());
    this.s.slides.on('click', (e) => this.handleSlideClick(e));
    this._initMobileEvents();
  }

  handleSlideClick(e) {
    this.slideTo($(e.currentTarget).index());
  }

  slideNext() {
    this.s.nextIndex = this.s.currentSlide + 1;
    (this.s.nextIndex >= this.s.slidesCount) && (this.s.nextIndex = 0);

    this.slideTo();
  }

  slidePrev() {
    this.s.nextIndex = this.s.currentSlide - 1;
    (this.s.nextIndex < 0) && (this.s.nextIndex = this.s.slidesCount - 1);

    this.slideTo();
  }

  slideTo(index, ignoreWait) {
    if (this.o.waitForTrue() || ignoreWait) {
      let ind = (index !== undefined) ? index : this.s.nextIndex,
          moveX = this._getMoveX(ind);
      //slide to px
      this._slide(moveX * this.o.RTL);

      //reset nextIndex once sliding initialized
      this.s.nextIndex = 0;

      //update currentSlide
      this.s.currentSlide = ind;

      //activate slide
      this._activateSlide(ind);
    }
  }

  _slide(to, stickToSlide) {
    let scrollTo = stickToSlide ? this._getClosestItemTranslate(to) : to;
    this.s.track.css('transform',`translate3d(${scrollTo}px,0,0)`);
  }

  _getClosestItemTranslate(to) {
    let closestItemTranslate = (-1) * this.s.slideWidth * Math.round(Math.abs(to) / this.s.slideWidth);
    closestItemTranslate = (closestItemTranslate >= -this.s.maxTranslate) ? closestItemTranslate : -this.s.maxTranslate;
    return closestItemTranslate * this.o.RTL;
  }

  _getMoveX(ind) {
    let moveX = this._indexIntoPx(ind);

    if (moveX < 0) return 0;
    if (moveX >= this.s.maxTranslate) return this.s.maxTranslate * (-1);
    return moveX * (-1);
  }

  _indexIntoPx(index) {
    return index * this.s.slideWidth;
  }

  _activateSlide(index) {
    let i = index || this.s.currentSlide;
    this.s.slides.removeClass('active').eq(i).addClass('active');
    this._onSlideChange(i);
    //callback
    this.o.onSlide(i);
  }

  _initMobileEvents() {
    let translate = 0,
        touch = {
          inAction: false,
          startSliderPos: 0,
          startPointerPos: 0,
          preLastTime: 0,
          preLastPos: 0,
          lastTime: 0,
          lastPos: 0
        },
        rtl = this.o.RTL;

    this.s.el.on('touchstart', (e) => {
      touch.inAction = false;
      translate = touch.startSliderPos = this._getCurrentTranslate();
      touch.startPointerPos = e.originalEvent.changedTouches[0].clientX;
    });

    this.s.el.on('touchmove', (e) => {
      this.s.el.removeClass('animate');
      touch.inAction = true;

      touch.preLastTime = touch.lastTime;
      touch.preLastPos = touch.lastPos;
      touch.lastTime = e.timeStamp;
      touch.lastPos = e.originalEvent.changedTouches[0].clientX;
      translate = touch.startSliderPos + touch.lastPos - touch.startPointerPos;

      //sliding out of available area
      if (translate * rtl > 0) {
        this._slide(translate/4);
      } else if (Math.abs(translate) > this.s.maxTranslate) {
        this._slide((-this.s.maxTranslate) * rtl + ((this.s.maxTranslate * rtl + translate) / 4));
      } else {
        this._slide(translate);
      }
    });

    this.s.el.on('touchend', () => {
      if (touch.inAction) {
        let time = touch.lastTime - touch.preLastTime,
            distance = touch.lastPos - touch.preLastPos,
            isNegative = distance < 0 ? -1 : 1,
            power = Math.abs(distance / time * 10),
            sp = power > 5 ? ((power * 1725 - 6225) / 32 * isNegative) : (power * 15 * isNegative);

        this.s.el.addClass('animate');

        let slideTo = translate + sp;

        //handle out of left screen edge
        if (translate * rtl > 0 || slideTo * rtl  > 0) {
          slideTo = 0;
        }

        //handle out of right screen edge
        if (!this.o.rtl) {
          if (translate < -this.s.maxTranslate || slideTo < -this.s.maxTranslate) {
            slideTo = -this.s.maxTranslate;
          }
        } else {
          if (translate > this.s.maxTranslate || slideTo > this.s.maxTranslate) {
            slideTo = this.s.maxTranslate;
          }
        }

        this._slide(slideTo, true);
      }
    });
  }

  _getCurrentTranslate() {
    let regExpr = new RegExp(/translate3d\((.*?)px/),
        style = this.s.track.attr('style'),
        test = regExpr.exec(style);

    if (test) return +test[1];

    return 0;
  }

}