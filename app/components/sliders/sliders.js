import { BaseComponent } from '../base-component.js';

/*
 * @description Introduces factory that creates a certain type of slider depending on a selector
 */
export class SlidersFactory {

  static get selector() { return '.js-slider, .adaptive-slider:not(.grid-slider), .grid-slider'; }

  get sliderTypes() {
    return {
      regular: 'js-slider',
      adaptive: 'adaptive-slider',
      grid: 'grid-slider'
    }
  }
  constructor(el) {
    this._init(el);
  }

  _init(el) {
    if ($(el).hasClass(this.sliderTypes.regular)) {
      new RegularSlider(el);
    }
    if ($(el).hasClass(this.sliderTypes.adaptive)) {
      new AdaptiveSlider($(el).find('>.row'));
    }
    if ($(el).hasClass(this.sliderTypes.grid)) {
      $(el).addClass('adaptive-slider');
      let rootSliderEl = $(el).find('>.row');
      rootSliderEl = rootSliderEl.length ? rootSliderEl : $(el);
      new AdaptiveSlider(rootSliderEl, true);
    }
  }

}

/*
 * @description Regular Lookbook(HPI like) slider, that is created for any .js-slider selector
 */
class RegularSlider extends BaseComponent {

  get defaults() {
    return $.extend({}, super.defaults, {
      infinite: true,
      interval: 5000,
      autostart: false,
      navType: 'both',
      readyClass: 'ready',
      bulletedDotsClass: 'bulleted-dots',
      rowSliderClass: 'row-slider',
      lightColorClass: 'font-color-theme1',
      enableArrowsMobileClass: 'enable-arrows-mobile',
      carouselCounterSelector: '.display-carousel-counter',
      amountSlides: 0
    });
  }

  constructor(...args) {
    super(...args);
    this._init();
  }

  _init() {
    this.counterEl = null;
    this._updateOptions();
    this._build();
    this._setCounter();
    this._onReady();
  }

  //Merge default options with data-options
  _updateOptions() {
    this.options = $.extend({}, this.options, this.el.data());

    //initialize autorotate manually via classname. Useful for autorotation with only ARROWS
    this.el.closest('.carousel-auto-rotate').length && (this.options.autostart = 'true');
  }

  _setCounter() {
    if (this.el.closest(this.options.carouselCounterSelector).length || this.el.find(this.options.carouselCounterSelector).length) {
      let slick = this.el.slick('getSlick');
      this.options.amountSlides = slick.slideCount;
      this.counterEl = $('<span />', { class: 'pagingInfo' });
      this.el.on('reInit afterChange', (e, slick, currentSlide) => this._updateCounter(currentSlide))
        .append(this.counterEl);

      this._updateCounter(slick.currentSlide);
    };
  }

  _updateCounter(currentSlide) {
    this.counterEl.text(`${currentSlide + 1}/${this.options.amountSlides}`);
  }

  _build() {
    this.el
      .on('init', this._afterChange)
      .on('beforeChange', this._beforeChange)
      .on('afterChange', this._afterChange)
      .slick({
        dots: this.options.navType === 'both' || this.options.navType === 'dotted',
        arrows: this.options.navType === 'both' || this.options.navType === 'arrow' || this.el.attr('data-autostart') === 'true',
        autoplay: this.options.autostart,
        touchMove: false,
        autoplaySpeed: this.options.interval,
        infinite: true,
        touchThreshold: 100
      });
  }

  _beforeChange(e, slick) {
    slick.$slides.eq(slick.currentSlide).find('.flex2-molecule').trigger('slider.viewportout');
  }

  _afterChange(e, slick) {
    slick.$slides.eq(slick.currentSlide).find('.flex2-molecule').trigger('slider.viewportin');
  }

  isIn800() {
    return this.el.parents("div[class*='-800']").length;
  }

  _checkParentTitle() {
    let mol800 = this.el.parents("div[class*='-800']");
    if (mol800.length && !mol800[0].hasAttribute("data-metrics-title")) {
      mol800.attr("data-metrics-title", "slider");
    }
  }

  _setMetrics() {
    if (this.el.slick('getSlick').$prevArrow) {
      this.leftArrow = this.el.slick('getSlick').$prevArrow;
      this.rightArrow = this.el.slick('getSlick').$nextArrow;
      this.leftArrow.attr({
        "data-metrics-title": "left-arrow",
        "data-metrics-link-type": "link"
      });
      this.rightArrow.attr({
        "data-metrics-title": "right-arrow",
        "data-metrics-link-type": "link"
      });
    }
    this._checkParentTitle();
  }

  _onReady() {
    this.el.find('.section.slick-slide').length && this.el.addClass(this.options.rowSliderClass);
    this.el.find('.section.font-color-theme1').length && this.el.addClass(this.options.lightColorClass);
    this.el.find('.section.preserve-row-content').length && this.el.addClass('preserve-row-content');
    this.el.find('.section.preserve-row-content-above-720').length && this.el.addClass('.preserve-row-content-above-720');
    this.el.find('.section.preserve-row-content-above-960').length && this.el.addClass('preserve-row-content-above-960');
    this.el.addClass(this.options.readyClass);

    //add bulleted dots apperance only if NO KW 'carousel-add-lines' is applied and NO autorotate.
    if (!this.el.closest('.carousel-add-lines').length && !this.options.autostart) {
      this.el.addClass(this.options.bulletedDotsClass);
    }

    /*if only ARROWS are visible on desktop or a 'carousel-arrows-on-mobile' KW is applied
      - show arrows on mobiles*/
    if (this.el.closest('.carousel-arrows-on-mobile').length || this.options.navType === 'arrow' || this.el.find('.carousel-arrows-on-mobile').length) {
      this.el.find('.slick-arrow').addClass(this.options.enableArrowsMobileClass);
    }

    if (this.isIn800()) {
      this._setMetrics();

      this.el.find('.slick-arrow').on("click", (e) => {
        GLOBALS.RegularMetrics.sendMetric($(e.currentTarget));
      });

      this.el.on('swipe', (event, slick, direction) => {
        let buttonTrack = direction == 'left' ? this.rightArrow : this.leftArrow;
        GLOBALS.RegularMetrics.sendMetric(buttonTrack);
      });
    }



    $(window).on("resize", () => _.debounce(() => this.el.slick('setPosition'), 100));
  }

}

/*
 * @description Adaptive Lookbook slider, that is created for any .adaptive-slider selector.
 *        Turns cells in a slider once breakpoint is matched
 */
class AdaptiveSlider extends BaseComponent {
  get defaults() {
    return {
      initializedClassname: 'slick-initialized',
      showNextPrevSlides: 'show-next-prev',
      initBelowBreakpoint: 720,
      readyClass: 'ready',
      disableInfinitySelector: '.noloop',
      initialSlideSelector: '[class*="initial-slide"]',
      bulletedDotsClass: 'bulleted-dots',
      enableArrowsMobileClass: 'enable-arrows-mobile',
      types: {
        regular: 'adaptive',
        custom: 'nextPrev',
        grid: 'grid'
      }
    };
  }

  constructor(...args) {
    super(...args);
    this.isGridSlider = args[1];
    this._init();
  }

  _init() {
    this.opts = {
      dots: true,
      touchMove: false,
      infinite: !this.el.parents(this.options.disableInfinitySelector).length,
      initialSlide: this._getInitialSlide(),
      touchThreshold: 100,
      slidesToShow: 1,
      slidesToScroll: 1,
      slide: 'div:not(.ignore)'
    }
    this.showNextPrevOpts = {
      centerMode: true,
      centerPadding: '28%',
      slidesToShow: 1,
      responsive: [{breakpoint: 1350,settings: {centerPadding: '20%'}},{breakpoint: 720,settings: {centerPadding: 0}}]
    }

    this.options.type = this._getSliderType();
    this._initSlider();
    this._onReady();

  }

  //detects initial slide index from classname initial-slide-XXX
  _getInitialSlide() {
    let initialIndex = 0,
      initialSlideIndicatorEl = this.el.parents(this.options.initialSlideSelector);

    if (initialSlideIndicatorEl.length) {
      initialIndex = /initial-slide-(\d{1,})/.exec(initialSlideIndicatorEl.attr('class'))[1];
      initialIndex = +initialIndex - 1
    }
    return initialIndex;
  }

  _getSliderType() {
    let type = this.isGridSlider ? this.options.types.grid : this.options.types.regular;
    (this.el.parent().hasClass(this.options.showNextPrevSlides)) && (type = this.options.types.custom);
    return type;
  }

  _initSlider() {
    switch (this.options.type) {
      case this.options.types.regular: {
        this._initRegularSlider();
        break;
      }
      case this.options.types.grid: {
        this._initGridSlider();
        break;
      }
      case this.options.types.custom: {
        this._initCustomSlider();
        break;
      }
    }
  }

  /**
   * regular adaptive slider - on screens > 720px looks like just static modules, and below -
   *                           turns into a slider
   */
  _initRegularSlider() {
    if (!this._isInitialized() && this._shouldBeInitialized()) {
      this._build();
      return;
    }
    if (this._isInitialized() && !this._shouldBeInitialized()) {
      this._destroy();
    }
  }

  /**
   * grid slider - a grid based slider. Is initialized for all the screens. Can show
   *               a defferent amount of slides in a single slide
   */
  _initGridSlider() {
    this.opts.slidesToShow = this.opts.slidesToScroll = this._getItemsPerSlideCount();
    if (this.opts.slidesToShow === 4) {
      this.opts.responsive = [{breakpoint: 1280,settings: {slidesToShow: 2,slidesToScroll: 2}}, {breakpoint: 720,settings: {slidesToShow: 1,slidesToScroll: 1}}];
    }
    if (this.opts.slidesToShow === 3) {
      this.opts.responsive = [{breakpoint: 960,settings: {slidesToShow: 1,slidesToScroll: 1}}];
    }
    if (this.opts.slidesToShow === 2) {
      this.opts.responsive = [{breakpoint: 720,settings: {slidesToShow: 1,slidesToScroll: 1}}];
    }
    if (this.el.closest('.grid-slider-autorotate').length) {
      this.opts.autoplay = true;
      this.opts.autoplaySpeed = this._getAutoplaySpeed() || 10000;
    }
    if (this.el.closest('.disable-manual-control').length) {
      this.opts.swipe = false;
    }
    if (this.el.closest('.transition-fade').length) {
      this.opts.fade = true;
    }
    if (this.el.closest('.adaptive-height').length) {
      this.opts.adaptiveHeight = true;
    }

    this._build();
  }

  _getAutoplaySpeed() {
    let result = /slider-interval-(.*?)(\D|$)/.exec(this.el.closest('.adaptive-slider').attr('class'));
    if (result) {
      return result[1];
    }
  }

  /**
   * custom slider - represents a pair of sliders with 'slider-nav' and 'slider-for' classes. Both 
   *                 sliders work syncronously. TODO - make pairs independent so there might be more
   *                 than 1 pair of sliders on a single page
   */

  _initCustomSlider() {
    //Paired sliders
    if (this.el.parent().hasClass('slider-nav') && $('.slider-for').length) {
      this.el.addClass('slider-nav').parent().removeClass('slider-nav');
      this.showNextPrevOpts = $.extend({}, this.showNextPrevOpts, {asNavFor: '.slider-for',dots: false});
    }
    if (this.el.parent().hasClass('slider-for') && $('.slider-nav').length) {
      this.el.addClass('slider-for').parent().removeClass('slider-for');
      this.showNextPrevOpts = $.extend({}, this.showNextPrevOpts, {asNavFor: '.slider-nav',arrows: false,speed: 0});
    }

    this.opts = $.extend({}, this.opts, this.showNextPrevOpts);
    this._build();
  }

  _getItemsPerSlideCount() {
    let regexpr = new RegExp(/span(\d*)/ig),
        slideClass = this.el.children().first().attr('class'),
        test = regexpr.exec(slideClass),
        count = (test && test[1]) ? 24 / (+test[1]) : 1;
      
    return count;
  }

  _isInitialized() {
    return this.el.hasClass(this.options.initializedClassname);
  }

  _shouldBeInitialized() {
    return window.innerWidth < this.options.initBelowBreakpoint;
  }

  _build() {
    this.el
        .on('init reInit breakpoint', () => this._onBuilt(arguments))
        .on('setPosition', () => this._onResize(arguments))
        .slick(this.opts);

    this.opts.initialSlide && setTimeout(() => this.el.slick('slickGoTo', this.opts.initialSlide));
  }

  _onBuilt() {
    if (!this.opts.autoplay) {
      this.el.addClass(this.options.bulletedDotsClass);
    }
    if (this.el.closest('.grid-slider-arrows-on-mobile').length || this.el.closest('.grid-slider-arrows-only').length) {
      this.el.find('.slick-arrow').addClass(this.options.enableArrowsMobileClass);
    }
    this.el.addClass(this.options.readyClass);
  }

  _onResize() {
    this.el.parent().trigger('update.equal.cells');
  }

  _destroy() {
    //in order to synchronize upading of equal-cells logic just make sure cells are recalculated on each build
    this.el.slick('unslick').parent().trigger('update.equal.cells');
  }

  _onReady() {
    (!this.isGridSlider) && this.el.addClass(this.options.readyClass);
    if (this.options.type === this.options.types.regular) {
      $(window).on('resize', _.debounce(() => this._initRegularSlider(), 100));
    }
  }
}