import { BaseComponent } from '../../base-component';

const YOUTUBE_API_ENDPOINT = 'https://www.youtube.com/iframe_api';

let youtubeAPILoaded = $.Deferred();

export class YoutubePlayer extends BaseComponent {

  static get PLAY_EVENT() { return 'youtube-player-play'; }
  static get PAUSE_EVENT() { return 'youtube-player-pause'; }
  static get STOP_EVENT() { return 'youtube-player-stop'; }

  get ytPlayerDefaults() {
    return {
      playerVars: {
        rel: 0
      }
    }
  }

  get dataAttributes() {
    return {
      videoId : 'data-id'
    };
  }

  get videoStates() {
    return {
      playerLastState: -1,
      UNSTARTED: -1,
      ENDED: 0,
      PLAYING: 1,
      PAUSED: 2,
      BUFFERING: 3,
      CUED: 5
    };
  }

  constructor(element, opts) {
    super(element);
    this.states = $.extend({}, this.videoStates);
    this._playerInitialized = $.Deferred();
    this._youtubePlayer = null;

    // Retrieve options from DOM element
    this.options = this._retrieveOptions(this.el, this.dataAttributes);

    //extend options by specified passed opts argument data
    this.options = $.extend({}, this.options, this.ytPlayerDefaults);

    opts && (this.options = $.extend({}, this.options, opts));
  }

  _retrieveOptions(element, attributes) {
    Object.keys(attributes).forEach((item) => {
      attributes[item] = element.attr(attributes[item]);
    });
    return attributes;
  }

  init(cb) {
    const videoElement = this.el[0];

    return loadAPIIfNeeded()
      .then(() => createNewPlayer(videoElement, this.options))
      .then((player) => {
        this._youtubePlayer = player;
        this._youtubePlayer.addEventListener("onStateChange", (data) => this._onPlayerStateChange(data));
        this._playerInitialized.resolve(this);
        cb && cb();
        return this._playerInitialized.promise();
      });
  }

  _onPlayerStateChange(event) {
    let videoDurationInSeconds = Math.floor((this._youtubePlayer.getDuration() - 0.01) || 0),
        videoQueuePointInSeconds = Math.round(this._youtubePlayer.getCurrentTime() || 0),
        videoTitle = this._youtubePlayer.getVideoData().title;
    try {
        if (this.states.playerLastState == this.states.UNSTARTED) {
            console.log('***********LB: Youtube player UNSTARTED **********', 'open' + ',' + videoTitle + ',' + videoDurationInSeconds);
            trackVideoMetrics('open', videoTitle, videoDurationInSeconds);
        }
        if (event.data == this.states.PLAYING) {
            console.log('***********LB: Youtube player PLAYING **********', 'play' + ',' + videoTitle + ',' + videoQueuePointInSeconds);
            trackVideoMetrics('play', videoTitle, videoQueuePointInSeconds);
        } else if (event.data == this.states.PAUSED) {
            console.log('***********LB: Youtube player PAUSED **********', 'stop' + ',' + videoTitle + ',' + videoQueuePointInSeconds);
            trackVideoMetrics('stop', videoTitle, videoQueuePointInSeconds);
        } else if (event.data == this.states.ENDED) {
            console.log('***********LB: Youtube player ENDED **********', 'close' + ',' + videoTitle);
            trackVideoMetrics('close', videoTitle);
        }
    } catch(e) {
        console.log(e.message);
    }
    if (event.data == this.states.ENDED) {
      this.states.playerLastState = this.states.UNSTARTED
    }  else {
      this.states.playerLastState = event.data;
    }
  }

  play(cb) {
    this._playerInitialized.then(() => {
      this._youtubePlayer.playVideo();
      this.broadcast(YoutubePlayer.PLAY_EVENT);
      cb && cb();
    });
  }

  pause() {
    this._playerInitialized.then(() => {
      this._youtubePlayer.pauseVideo();
      this.broadcast(YoutubePlayer.PAUSE_EVENT);
    });
  }

  stop() {
    this._playerInitialized.then(() => {
      this._youtubePlayer.stopVideo();
      this.broadcast(YoutubePlayer.STOP_EVENT);
    });
  }
}

function loadAPIIfNeeded() {
  if (!window.YT) {
    window.onYouTubeIframeAPIReady = () => youtubeAPILoaded.resolve();
    $.getScript(YOUTUBE_API_ENDPOINT);
  } else {
    if (!window.YT.loaded) {
      window.onYouTubeIframeAPIReady = () => youtubeAPILoaded.resolve();
    } else {
      youtubeAPILoaded.resolve();
    }
  }
  return youtubeAPILoaded.promise();
}

function createNewPlayer(element, options) {
  var playerReady = $.Deferred();

  options.events = options.events || {};
  options.events.onReady = (event) => {
    playerReady.resolve(event.target);
  };
  options.events.onError = () => playerReady.reject();
  console.log(options);
  new window.YT.Player(element, options);

  return playerReady.promise();
}
