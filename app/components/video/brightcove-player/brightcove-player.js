import { BaseComponent } from '../../base-component';

let bcAPILoaded = $.Deferred();

export class BrightcovePlayer extends BaseComponent {

  get defaultData() {
    return {
      "data-embed":"default",
      "controls": ""
    }
  }
  constructor(element, opts) {
    super(element);
    this._playerInitialized = $.Deferred();
    this._captionsAvailable = $.Deferred();
    this._bcPlayer = null;
    this._captions = false;
    this._playsInline = opts ? opts.playsInline : false;
    this._videoUniqueId = _.uniqueId('bc-id-');
  }

  init(cb) {
    this._handleCaptions();
    this._updateVideoId();
    this._preparePlayerHtml();

    return this.loadAPIIfNeeded()
      .then(() => this.createNewPlayer())
      .then((player) => {
        this._bcPlayer = player;
        this._playerInitialized.resolve(this);
        this._initMetrics();
        this._bcPlayer.on('play', () => {
          this._captionsAvailable.resolve();
        });
        cb && cb();
       
        return this._playerInitialized.promise();
      });
  }

  /**
   * Old video ids, which contain alpha-numeric symbols should have a preffix of 'ref:'
   */
  _updateVideoId() {
    let videoId = this.el.attr('data-video-id'),
        alphaRegex = new RegExp('(?:[a-z0-9]+\-)+[a-z0-9]+$','gi');

    if (alphaRegex.exec(videoId)) {
      videoId = 'ref:' + videoId;
    }
    this.el.attr('data-video-id', videoId);
  }

  _preparePlayerHtml() {
    let playerAttrs = $.extend({}, this.defaultData, this._getParams()),
        playerEl = $('<video />', {'id': this._videoUniqueId, 'class': "video-js"});

    for (let i in playerAttrs) {
      playerEl.attr(i, playerAttrs[i]);
    }
    let parent = this.el.parent();
    this.el.replaceWith(playerEl);

    this.el = playerEl;
    this.el.parent = parent;
    this.accountId = playerAttrs['data-account'];
    this.playerId = playerAttrs['data-player'];
  }

  _handleCaptions() {
    this._captions = this.el.attr('data-captions') === 'true';
    this._captions && this._captionsAvailable.then(() => {
      this._showCaptions();
    });
  }

  _getParams() {
    //fallback to support already existed HTML5 BC videos with OLD data format.
    if (this.el.attr('data-video')) {
      return GLOBALS.Utils._parseQuery(this.el.attr('data-video'))
    }
    let requiredParams = [{
          name: 'account',
          val: 'account'
        }, {
          name: 'player',
          val: 'playerID'
        }, {
          name: 'video-id',
          val: 'video-id'
        }],
        params = {};

    requiredParams.forEach((param, index) => {
      params['data-' + param.name] = this.el.attr('data-' + param.val);
    });
    return params;
  }

  loadAPIIfNeeded() {
    let APILoaded = $.Deferred(),
        APISrc = this._getPlayerInitScript();

    $.getScript(APISrc, () => {
      APILoaded.resolve();
    });
    return APILoaded.promise();
  }

  createNewPlayer(element, options) {
    var playerReady = $.Deferred();
    
    if (typeof videojs != 'undefined') {
      videojs(this._videoUniqueId).ready(function() {
        //NOTE, don't change to arrow syntax, this is required
        playerReady.resolve(this);
      });
    };

    return playerReady.promise();
  }

  _getPlayerInitScript() {
    return "//players.brightcove.net/" + this.accountId + "/" + this.playerId + "_default/index.min.js";
  }

  _showCaptions() {
    let tracks = this._bcPlayer.textTracks();
    for (let i = 0; i < tracks.length; i++) {
      let track = tracks[i];
      if (track.kind === 'captions' && this._trackLangMatchesPage(track.language)) {
        track.mode = 'showing';
      }
    }
  }

  _trackLangMatchesPage(language) {
    if (language) {
      let trackLang = language.toLowerCase(),
          trackLangSplited = trackLang.split('-'),
          pageLang = $('html').attr('lang') ? $('html').attr('lang').toLowerCase() : 'en-us';

      if (trackLangSplited.length > 1) {
        return trackLang === pageLang;
      }
      return trackLangSplited[0] === pageLang.split('-')[0];
    }
  }

  _getCurrentLanguage() {
    let lang = $('html').attr('lang') || 'en';
    return lang.split('-')[0];
  }

  play(cb) {
    this._playerInitialized.then(() => {
      this._bcPlayer.play();
      cb && cb();
    });
  }

  pause() {
    this._playerInitialized.then(() => this._bcPlayer.pause());
  }

  stop() {
    this._playerInitialized.then(() => {
      this._bcPlayer.pause();
      this._bcPlayer.currentTime(0.01);
    });
  }

  _initMetrics() {
    let events = ['playing', 'pause', 'ended'],
        videoTitle = '',
        videoDurationInSeconds,
        videoQueuePointInSeconds,
        hasBeenEnded = true;

    //duration is available only once metadata is loaded
    this._bcPlayer.on('loadedmetadata', () => {
      videoDurationInSeconds = Math.floor(this._bcPlayer.duration() || 0);
      videoTitle = this._bcPlayer.mediainfo ? this._bcPlayer.mediainfo.name : '';
    });

    //attach events to capture
    events.forEach((event) => {
      this._bcPlayer.on(event, (event) => handleEvent.call(this, event));
    });

    function handleEvent(event) {
      //handle events only if metadata is loaded
      if (videoDurationInSeconds) {
        videoQueuePointInSeconds = Math.floor(this._bcPlayer.currentTime() || 0);
        switch (event.type) {
          case 'playing': {
            //fix for quick play-pause on a video init
            if (!this._playsInline 
              && !this.el.parent.closest('.overlay-popup').hasClass('opened') 
              && !this.el.parent.closest('.playing-video').length) {
              this.pause();
            };

            //if video has beed ended - then fire 'open' before next 'play' event
            if (hasBeenEnded) {
              hasBeenEnded = false;
              send('open', videoTitle, videoDurationInSeconds);
            }
            send('play', videoTitle, videoQueuePointInSeconds);
            break;
          }
          case 'pause': {
            //prevent sending 'stop' event on video 'ended' event
            (videoQueuePointInSeconds != videoDurationInSeconds) && send('stop', videoTitle, videoQueuePointInSeconds);
            break;
          }
          case 'ended': {
            /* if video has beed ended - change flag hasBeenEnded to true, to be able trigger 'open'
               before the next 'play event'*/
            hasBeenEnded = true;
            send('close', videoTitle, videoQueuePointInSeconds);
            break;
          }
        }
      }
    }

    function send(eventType, title, duration) {
      console.log(eventType, title, duration);
      try {
        trackVideoMetrics(eventType, title, duration);
      } catch(e) {
        console.log('Metrics are NOT sent: ' + e.message);
      }
    }
  }
}