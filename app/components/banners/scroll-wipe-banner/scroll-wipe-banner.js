import {BaseComponent} from '../../base-component.js';

export class ScrollWipeBanner extends BaseComponent {

  static get selector() {
    return '.scroll-wipe-banner';
  }

  get defaults() {
    return $.extend({}, super.defaults, {
      offsetLeftRight: 10, //px between edges and blue draggable element
      initialWidth: 75, //percentage [offsetLeftRight..100%-offsetLeftRight]
      offEvents: 'mouseup.swipebanner touchend.swipebanner',
      responsiveMedia: window.matchMedia('screen and (max-width: 719px)')
    });
  }

  constructor(...args) {
    super(...args);
    this._init();
  }

  _init() {
    //currentPos is always updated
    this.offset = 40; //default offset
    this.scalableElDesktop = this.el.find('[class*="show-above"].molecule-lb-501').last();
    this.scalableElMobile = this.el.find('[class*="show-below"].molecule-lb-501').last();
    this._setInitialPosition();
    this.mover = this._addMover();
    this._updateMoverHeight(this.options.responsiveMedia.matches);
    this._updateOffset();
    this._move();
    this._initGeneralListeners();
    this._updateMoveListeners();
    this._onReady();
  }

  /**
   * @description picks up the width from the image ONLY if it's value is less, than 100, 
   *              assuming this value is set by editors in percents of initial position
   */
  _setInitialPosition() {
    let imgWidth = +this.scalableElDesktop.find('img').attr('width');

    this.currentPos = (imgWidth < 100) ? imgWidth : this.options.initialWidth;
  }

  _addMover() {
    let mover = $('<div />', {'class': 'mover'}),
        moverDragArea = $('<div />', {'class': 'drag-area'});

    this.el.append(mover.append(moverDragArea));
    return mover;
  }

  _updateMoverHeight(isMobileBp) {
    let browser = isMobileBp ? 'Mobile' : 'Desktop',
        height = +this[`scalableEl${browser}`].find('img').attr('height');

    height = (height <= 100) ? height : 100;
    this.mover.height(`${height}%`);
  }

  _move() {
    this.mover.css('left', `${this.currentPos}%`);
    this.scalableElDesktop.css('width', `${this.currentPos}%`);
    this.scalableElMobile.css('width', `${this.currentPos}%`);
  }

  _initGeneralListeners() {
    //this.offset should be recalculated on window.load due to browser issue when reloading page with F5
    $(window).on('resize', _.debounce(() => this._updateOffset(), 50))
             .on('load', () => this.offset = this.options.offsetLeftRight + this.el.find('.drag-area').width() / 2);

    this.options.responsiveMedia.addListener((media) => this._updateMoverHeight(media.matches));
  }

  _updateMoveListeners() {
    let isMovable = false,
        interactionStartX,
        moverStartX;

    if (GLOBALS.Utils.device.isMobile) {
      this.el.on('touchstart', (e) => handleStart.call(this, e.originalEvent.changedTouches[0].clientX))
             .on('touchmove', (e) => handleMove.call(this, e.originalEvent.changedTouches[0].clientX));
    } else {
      this.mover.on('mousedown', (e) => handleStart.call(this, e.clientX));
      this.el.on('mousemove', (e) => handleMove.call(this, e.clientX));
    }

    //general mousedown/touchstart listener
    function handleStart(clientX) {
      isMovable = true;
      interactionStartX = clientX;
      moverStartX = this._percentsToPx(this.currentPos);
      $(window).off(this.options.offEvents).one(this.options.offEvents, (e) => isMovable = false);
    }

    //general mousemove/touchmove listener
    function handleMove(clientX) {
      if (isMovable) {
        this.currentPos = this._pxToLimitedPercents(moverStartX + clientX - interactionStartX);
        this._move();
      }
    }

  }

  /**
   * @Description updates this.offset (px) value, which is a spacing between blue drag-area element 
   *              and edges of the browser. Is called on each resize to prevent drag-area el from overflowing
   *              browser's edges. Performs move for correcting spacings
   */
  _updateOffset() {
    this.winWidth = $(window).width();
    this.currentPos = this._pxToLimitedPercents(this._percentsToPx(this.currentPos));
    this.offset = this.options.offsetLeftRight + this.el.find('.drag-area').width() / 2;
    this._move();
  }

  _onReady() {
    this.el.addClass('initialized');
  }

  _pxToLimitedPercents(valuePx) {
    (valuePx < this.offset) && (valuePx = this.offset);
    (valuePx > this.winWidth - this.offset) && (valuePx = this.winWidth - this.offset);
    return valuePx * 100 / this.winWidth;
  }

  _percentsToPx(valuePercents) {
    return valuePercents * this.winWidth / 100;
  }
}
