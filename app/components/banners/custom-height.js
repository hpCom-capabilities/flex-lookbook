import { BaseComponent } from '../base-component.js';

export class CustomHeight extends BaseComponent {

  static get selector() { return '.section.custom-height:not(.parallax-panel)'; }

  constructor(...args) {
    super(...args);
    this._init();
  }

  _init() {
    this._initCustomHeight();
  }

  _initCustomHeight() {
    //pick up inline height from .section and replace it with min-height 
    const style = this.el.attr('style');
    style && this.el.attr('style', style.replace(/height/ig, 'min-height'));
  }
}

