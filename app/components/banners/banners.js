import { BaseComponent } from '../base-component.js';

export class Banners extends BaseComponent {

  static get selector() { return '.section.full[style*="url"][data-secondary-background]:not(.custom-master):not(.custom-slave)'; }

  get defaults() {
    return $.extend({}, super.defaults, {
      DEFAULT_FLIP_BP: '720',
      //NOTE, it is not a real KW, and used only for custom logic now
      preserveImagesRatio: 'preserve-image-ratio'
    });
  }
  
  constructor(...args) {
    super(...args);
    this._init();
  }

  _init() {
    this._initBgFlip();
  }

  _initBgFlip() {
    let customBP = this._getWrapBreakpoint(),
      mobileQuery = window.matchMedia('screen and (max-width: ' + customBP + 'px)'),
      url = this.el.css('background-image').replace(/url\(['"]*(.*?)['"]*\)/, '$1');
    this.el.attr('data-primary-background', url);

    //sometimes both Desktop and Mobile images should preserve their ratio
    if (this.el.hasClass(this.options.preserveImagesRatio)) {
      this.bgImageHelper = $('<img />', {'class': 'bg-image-helper', 'alt': 'background image'});
      this.el.append(this.bgImageHelper);
    }
    this._changeBackgrounds(mobileQuery);
    mobileQuery.addListener(this._changeBackgrounds.bind(this));
  }

  _changeBackgrounds(mql) {
    let image = mql.matches ? 'data-secondary-background' : 'data-primary-background';
    if (!this.bgImageHelper) {
      this.el.css('background-image', 'url("'+ this.el.attr(image) + '")');
    } else {
      this.bgImageHelper.attr('src', this.el.attr(image));
    }
  }

  _getWrapBreakpoint() {
    let result = /flip-bgs-breakpoint-(.*?)(\D|$)/.exec(this.el.attr('class'));
    if (result) {
      return result[1];
    }
    return this.options.DEFAULT_FLIP_BP;
  }

}

