/*This file is not in use for now */

'use strict';

/* Initializes external BuyNow API
 * https://admin.metalocator.com/ops/tests/hp.wtbhtml.html
 **/
export class BuyNow {
  
  get options() {
    return {
      selector : 'a[href^="?ML?"]',
      scriptTpl: ''+
        '<script type="text/javascript" data-metalocator-root="1">'+
          'var MetaLocator = window.MetaLocator || {};'+
          '(function (window, document) {'+
            'var loader = function () {'+
              'var script = document.createElement("script"), tag = document.getElementsByTagName("script")[0];'+
              'var secure = window.location.protocol === "https:";'+
              'script.src = (secure ? "https" : "http") + "://admin.metalocator.com/app/js/button.js?rev=0.2";'+
              'script.setAttribute("data-metalocator-head",1);'+
              'tag.parentNode.insertBefore(script, tag);'+
            '};'+
            'window.addEventListener ? window.addEventListener("load", loader, false) : window.attachEvent("onload", loader);'+
          '})(window, document);'+
        '</script>'
    }
  }

  constructor() {
    this._init();
  }

  _init() {
    $(this.options.selector).each((index, element) => {
      let el = $(element),
        hrefParams = GLOBALS.Utils._parseQuery(el.attr('href').replace('?ML', '')),
        linkText = el.html(),
        linkAttrs = element.attributes,
        linkHtml = '<a';

      //link creation implemented such a way cause it's not possible to set data-test[products] attribute
      //to link, cause it assumed to be invalid

      //add attrs from parsed Query in href
      for (let i in hrefParams) {
        linkHtml += ' ' + i + '=' + '"' + hrefParams[i] + '"';
      }
      //add original attrs to link
      Array.prototype.forEach.call(linkAttrs, (el, index) => {
        if (el.name !== 'href') {
          linkHtml += ' ' + el.name + '="' + el.value + '"';
        }
      });
      linkHtml += '>' + linkText + '</a>';
      console.log(linkHtml);
      el.replaceWith(linkHtml);
    });
    $('head').append(this.options.scriptTpl);
  }
}
