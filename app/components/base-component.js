import { eventBus } from '../common/scripts/event-bus';

export class BaseComponent {
  get defaults() {
    return {};
  }

  constructor(element, options) {
    this.el = $(element);
    this.options = $.extend({}, this.defaults, options);

    if (this.ROOT_CLASS) {
      this.el.addClass(this.ROOT_CLASS);
    }
  }

  on(eventName, handler) {
    this.el.on(eventName, handler);
    // Returning unbind function
    return () => this.off(eventName, handler);
  }

  off(eventName, handler) {
    this.el.off(eventName, handler);
  }

  trigger(eventName, ...data) {
    this.el.trigger(eventName, [...data]);
  }

  broadcast(eventName, ...data) {
    this.trigger(eventName, ...data);
    eventBus.trigger(eventName, this, ...data);
  }

  onResize(cb, delay, method='debounce') {
    let handler = _.debounce(cb, debounceDelay);
    $(window).on('resize', handler);
    return () => $(window).off('resize', handler);
  }
}
