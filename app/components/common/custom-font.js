'use strict';

import { BaseComponent } from '../../components/base-component.js';

export class CustomFont extends BaseComponent {

  static get selector () { return '[style*="tablet"]:not(.section), [style*="mobile"]:not(.section)'; }

  get defaults() {
    return {
      medias: {
        desktop: 'screen and (min-width: 1281px)',
        tablet: 'screen and (max-width: 1280px) and (min-width: 721px)',
        mobile: 'screen and (max-width: 720px)'
      }
    }
  }

  constructor(...args) {
    super(...args);
    this._init();
  }

  _init() {
    this._retrieveCustomStyles();
    this._updateDataAttrs();
    for (let i in this.options.medias) {
      let media = window.matchMedia(this.options.medias[i]);
      media.addListener((media) => this.update(media));
      this.update(media);
    }
  }

  _retrieveCustomStyles() {
    let style = this.el.attr('style');
    if (style) {
      let desktop = style.replace(/\/\*(.*?)\*\//ig, ''),
        tablet = style.match(/\/\*tablet(.*?)\*\//),
        mobile = style.match(/\/\*mobile(.*?)\*\//);

      tablet = tablet ? tablet[1] : '';
      mobile = mobile ? mobile[1] : '';
      desktop && this.el.attr('data-style-desktop', desktop);
      tablet && this.el.attr('data-style-tablet', tablet);
      mobile && this.el.attr('data-style-mobile', mobile);
    }
  }

  _updateDataAttrs() {
    !this.el.attr('data-style-desktop') && this.el.attr('data-style-desktop', '');
    if (!this.el.attr('data-style-mobile') && this.el.attr('data-style-tablet')) {
      this.el.attr('data-style-mobile', this.el.attr('data-style-tablet'));
    }
  }

  update(media) {
    if (media.matches) {
      switch (media.media) {
        case this.options.medias.desktop: {
          this.setStyle('desktop');
          break;
        }
        case this.options.medias.tablet: {
          this.setStyle('tablet');
          break;
        }
        case this.options.medias.mobile: {
          this.setStyle('mobile');
          break;
        }
      }
    }
  }

  setStyle(screen) {
    let style = this.el.data('style-' + screen) || this.el.data('style-desktop');
    this.el.attr('style', style);
  }
}