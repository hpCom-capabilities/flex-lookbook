import { BaseComponent } from '../../components/base-component.js';
import { Stickable } from '../../common/scripts/stickable.js';


export class Molecule836Theme2 extends BaseComponent {

  static get selector() { return '.molecule-lb-836.appearance-theme-2'; }

  get defaults() {
    return $.extend({}, super.defaults, {
      rowSelector: '.section',
      rowClass: 'section-with-tabs-theme-2',
      stickyWrapperSelector: '.sticky-wrapper',
      contentClass: 'tab-content content-theme-2',
      activeClass: 'tab-content-active',
      activeTabClass: 'active-tab',
      contentSelector: '.flex-content',
      linksContainerSelector: '.tabs-container',
      linksSelector: 'a',
      sliderSelector: '.slider-carousel-arrows-and-dots',
      underlineSliderTpl: '<div class="underline-slider">' +
        '<span class="switcher"></span>' +
        '</div>'
    });
  }

  constructor(...args) {
    super(...args);
    this._init();
  }

  _init() {
    this._addUnderlineSlider();
    this._wrapForScrolling();
    this._collectContents();
    this._wrapLinks();
    this.isScrollable = false;
    this._initListeners();
    this._wrapSticky();
    this._initStickableBehaviour();
    this._checkScrollable();
    this.processContentsBackground();
    this.tabHashes = {};

    this.links.each((index, link) => {
      if (link.hash) {
        this.tabHashes[link.hash] = true;
      }
    });

    if (!this._openTabByHash()) {
      this.links.first().trigger('click', { initialClick: true });
    }
  }

  //here detecting tabs sections with content
  _collectContents() {
    this.container = this.el.find(this.options.linksContainerSelector);
    this.links = this.container.find(this.options.linksSelector);
    this.contents = this.el
      .parents(this.options.contentSelector)
      .nextAll(this.options.contentSelector)
      .slice(0, this.links.length)
      .addClass(this.options.contentClass);

    this.el.closest(this.options.rowSelector).addClass(this.options.rowClass);
  }

  _wrapLinks() {
    this.links.each((index, el) => {
      $(el).wrapInner("<span></span>");
    });
  }

  _openTabByHash(hash, forceScroll) {
    var tab = this._findTabByHash(hash);

    if (!tab.length) {
      return false;
    }

    if (!tab.parent().hasClass(this.options.activeTabClass)) {
      tab.each((i, $el) => {
        this._updateActiveTab($el);
        this._updateActiveContent($el);
        this._updateUnderline($el);
      });
    }

    var target = $(this.el),
      stickyNavBefore = this._getPrevStickyNavHeight() || 0,
      topNumber = target.offset().top - stickyNavBefore;
    if (!target.hasClass('no-initial-scroll') || forceScroll) {
      window.setTimeout(function() {
        GLOBALS.ScrollTo.navigateTo(topNumber, 1000);
      }, 300);
    }

    return true;
  }

  _findTabByHash(hash) {
    hash = hash || window.location.hash;
    return $($.grep(this.tabTriggers, function(item) {
      return hash && (item.hash === hash);
    }));
  }

  _addUnderlineSlider() {
    let tpl = _.template(this.options.underlineSliderTpl);
    this.el.find('ul').append(tpl);
  }

  _wrapForScrolling() {
    this.el.find('ul').wrap('<div class="scrolling-panel"></div>');
    this.scrollableDiv = this.el.find('.scrolling-panel');
    this.scrollableEl = this.scrollableDiv.find('.tabs-container');
  }

  _checkScrollable() {
    let parentUl = this.el.find('ul')
    if (document.body.clientWidth < parentUl.width()) {
      this.isScrollable = true;
    } else {
      this.isScrollable = false;
    }
  }

  _getPrevStickyNavHeight() {
    //flexContentParent is true when 836,830,835 are inside different .flex-content wrappers
    //sectionParent is true when 836,830,835 are inside one .flex-content and different .sections
    let prevParent,
      flexContentParent = this.el.parents(this.options.contentSelector).prevAll(this.options.contentSelector).not('.tab-content'),
      sectionParent = this.el.parents('.section').prevAll('.section');
    if (flexContentParent.length) {
      prevParent = flexContentParent;
    } else {
      prevParent = sectionParent;
    }
    let prevParentNear = prevParent.children().find(".molecule-lb-835, .molecule-lb-836, .molecule-lb-830").not(".non-stickable"),
      maxHeight = 0;

    if (prevParentNear.length > 0) {
      maxHeight = $(prevParentNear.eq(0)).height();
    }
    return maxHeight;
  }

  _initListeners() {
    //var self = this,
    let  $htmlOrBody = $('html, body');

    this.tabTriggers = this.el.find('a');

    // Any link on a page may be targeting tabs
    $('body').on('click', 'a', (e) => {
      let link = e.currentTarget;
      if (this.tabTriggers.index(link) == -1) {
        if (link.hash && (link.hash in this.tabHashes)) {
          if (link.pathname === window.location.pathname) {
            this._openTabByHash(link.hash, true);
            window.location.hash = link.hash;
            e.preventDefault();
          }
        }
      }
    });

    this.tabTriggers.on('click', (e, params) => {
      let clicked = $(e.currentTarget);
      this._updateActiveTab(clicked);
      this._updateActiveContent(clicked);
      this._updateUnderline(clicked);

      if (!params || !params.initialClick) {
        if (clicked.attr('href').length > 1) {
          window.location.hash = clicked.attr('href');
        }
        var target = $(this.el),
          stickyNavBefore = this._getPrevStickyNavHeight() || 0,
          topNumber = target.offset().top - stickyNavBefore;

        setTimeout(() => GLOBALS.ScrollTo.navigateTo(topNumber, 1000), 0);
      }

    });

    this.tabTriggers.on('mouseenter', (e, params) => {
      let focused = $(e.currentTarget);

      if (this.isScrollable) {
        setTimeout(() => {
          this._scrollTo(focused);
        }, 500);
      }
    });


    $(window).on("resize", _.debounce(() => {
      let activeLink = this.el.find("." + this.options.activeTabClass).find("a");
      this._checkScrollable();
      this._updateMargins();
      this._centerActiveItem(activeLink);
    }, 250));

    $(window).on("load", _.debounce(() => {
      let activeLink = this.el.find("." + this.options.activeTabClass).find("a");
      this._checkScrollable();
      this._updateMargins();
      this._centerActiveItem(activeLink);
      this._updateUnderline(activeLink);
    }, 500));

    $htmlOrBody.on([
      'scroll',
      'mousedown',
      'wheel',
      'DOMMouseScroll',
      'mousewheel',
      'keyup',
      'touchmove'
    ].join(' '), function stopTabsAnimations() {
      $htmlOrBody.stop();
    });
  }

  _updateActiveTab(el) {
    let activeTab = $(el),
        index = this.links.index(activeTab);

    if (index != -1) {
      this.tabTriggers
        .parents('li')
        .removeClass(this.options.activeTabClass);
      $(this.links.parent('li').eq(index))
        .addClass(this.options.activeTabClass);
    }
  }

  _updateActiveContent(el) {
    let clickedLink = $(el),
        index = this.links.index(clickedLink);

    if (index != -1) {
      this.contents
        .removeClass(this.options.activeClass)
        .eq(index)
        .addClass(this.options.activeClass)
        .trigger('tab.activated')
        .find('.slick-slider').slick('setPosition');;
    }
  }

  _updateUnderline(el) {
    let clickedLink = $(el),
      index = this.links.index(clickedLink),
      switcher = this.el.find('.underline-slider .switcher'),
      parentUl = this.el.find('ul'),
      widthTarget = parseInt(clickedLink.width()) + "px",
      position,
      offsetTarget,
      marginType;

    if(GLOBALS.Utils.isRTL()) {
      position = 'right';
      offsetTarget = parseInt(parentUl[0].getBoundingClientRect()[position] - clickedLink[0].getBoundingClientRect()[position] - parentUl.css("padding-left").replace("px", "")) + "px",
      marginType = "margin-right";
    } else {
      position = 'left';
      offsetTarget = parseInt(clickedLink[0].getBoundingClientRect()[position] - parentUl[0].getBoundingClientRect()[position] - parentUl.css("padding-left").replace("px", "")) + "px";
      marginType = "margin-left";
    }

    if (index != -1) {
      switcher.css("width", widthTarget).css(marginType, offsetTarget);
    }
  }

  _scrollTo(el) {
    let $el = $(el),
      posLeft;

    posLeft = parseInt($($el).offset().left - ((this.scrollableDiv.width() - $el.outerWidth(true)) / 2) + this.scrollableDiv.scrollLeft());

    this.scrollableEl.velocity('stop').velocity('scroll', {
      container: this.scrollableDiv,
      axis: 'x',
      offset: posLeft,
      duration: 300
    });
  }

  _getCenter(el) {
    let $el = $(el),
      centering = parseInt(document.body.clientWidth / 2 - $el.width() / 2);
    return centering;
  }

  _updateMargins() {
    let parentUl = this.el.find("ul"),
      lastElIndex = parentUl.find("li").last().index(),
      spaceForCentering,
      paddingProp,
      marginProp,
      marginSpace,
      items = parentUl.find("li");

    if (this.isScrollable) {
      items.each(function(index, el) {
        if (index == 0) {
          paddingProp = "padding-left";
          marginProp = "margin-left";
          spaceForCentering = this._getCenter(el);
        } else if (index == lastElIndex) {
          paddingProp = "padding-right";
          marginProp = "margin-right";
          spaceForCentering = this._getCenter(el);
        } else {
          return;
        }

        marginSpace = parseInt(spaceForCentering - parentUl.css(paddingProp).replace("px", "")) + "px";
        parentUl.css(marginProp, marginSpace);

      }.bind(this));

    } else {
      $(parentUl).css("margin", 0);
    }

  }

  _centerActiveItem(item) {
    let activeLink = $(item);

    setTimeout(() => {
      this._scrollTo(activeLink);
    }, 300);
  }

  _wrapSticky() {
    $(this.el).wrapInner('<div class="sticky-wrapper"></div>');
  }

  _initStickableBehaviour() {
    if (!this.el.hasClass('non-stickable')) {
      new Stickable(this.el.find(this.options.stickyWrapperSelector));
    }
  }

  processContentsBackground() {
    this.contents.each((index, el) => {
      var $tabContent = $(el),
        sectionInSlider = $tabContent.find(this.options.sliderSelector),
        className = '';

      if (sectionInSlider.length && sectionInSlider[0].className.indexOf('bkg-color-') > -1) {
        sectionInSlider.each(function() {
          var classes = this.className.split(' ');
          for (var i = 0; i < classes.length; i++) {
            if (classes[i].indexOf('bkg-color-') > -1) {
              className = classes[i];
            }
          }
        });
        $tabContent.find('.section').addClass(className);
      }
    });
  }
}
