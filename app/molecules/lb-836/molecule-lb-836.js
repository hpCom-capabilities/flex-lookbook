import { BaseComponent } from '../../components/base-component.js';
import { Stickable } from '../../common/scripts/stickable.js';


export class Molecule836 extends BaseComponent {

  static get selector() { return '.molecule-lb-836:not(.appearance-theme-2)'; }

  get defaults() {
    return $.extend({}, super.defaults, {
      rowSelector: '.section',
      rowClass: 'section-with-tabs',      
      stickyWrapperSelector: '.sticky-wrapper',
      contentClass: 'tab-content',
      activeClass: 'tab-content-active',
      activeTabClass: 'active-tab',
      contentSelector: '.flex-content',
      linksContainerSelector: '.tabs-container',
      linksSelector: 'a',
      activeMobile: 'mob-active',
      linksContainerMobile: '.mobile-tabs-container .drop',
      sliderSelector: '.slider-carousel-arrows-and-dots',
      mobileTpl: '<div class="mobile-tabs">' +
        '<div class="mobile-tabs-container">' +
        '<%= dropTrigger %>' +
        '<div class="drop"><ul><%= items %></ul></div>' +
        '</div>' +
        '</div>'                                                
    });
  }

  constructor(...args) {
    super(...args);
    this._init();
  }

  _init() {
    
    this._prepareMobile();        
    this._collectContents();
    this._wrapLinks();
    this.isMobile = false;
    this._initListeners();
    this._wrapSticky();
    this._initStickableBehaviour();
    this._clickOutsideCb = this._clickOutsideCb.bind(this);
    this._checkMobile();
    this.processContentsBackground();
    this.tabHashes = {};

    this.links.each((index, link) => link.hash && (this.tabHashes[link.hash] = true));

    if (!this._openTabByHash()) {
      this.links.first().trigger('click', { initialClick: true });
      this.linksMobile.first().trigger('click', { initialClick: true });
    }
  }

  //here detecting tabs sections with content
  _collectContents() {
    this.container = this.el.find(this.options.linksContainerSelector);
    this.links = this.container.find(this.options.linksSelector);
    this.containerMobile = this.el.find(this.options.linksContainerMobile);
    this.linksMobile = this.containerMobile.find(this.options.linksSelector);
    this.contents = this.el
      .parents(this.options.contentSelector)
      .nextAll(this.options.contentSelector)
      .slice(0, this.links.length)
      .addClass(this.options.contentClass);        
    this.el.closest(this.options.rowSelector).addClass(this.options.rowClass);    
  }

  _wrapLinks() {
    this.links.each((index, el) => {
      $(el).wrapInner("<span></span>");
    });
  }

  _openTabByHash(hash, forceScroll) {
    let tab = this._findTabByHash(hash)

    if (!tab.length) {
      return false;
    }

    if (!tab.parent().hasClass(this.options.activeTabClass)) {
      tab.each((i, $el) => {
        this._updateActiveTab($el);
        this._updateActiveContent($el);
      });
    }

    let target = $(this.el),
        stickyNavBefore = this._getPrevStickyNavHeight() || 0,
        topNumber = target.offset().top - stickyNavBefore;

    if (!target.hasClass('no-initial-scroll') || forceScroll) {
      window.setTimeout(() => GLOBALS.ScrollTo.navigateTo(topNumber, 1000), 200);
    }
    return true;
  }

  _findTabByHash(hash) {
    hash = hash || window.location.hash;
    return $($.grep(this.tabTriggers, function(item) {
      return hash && (item.hash === hash);
    }));
  }
 
  _prepareMobile() {
    let tpl = _.template(this.options.mobileTpl),
      items = this.el.find('ul').clone(true).html(),
      firstTab = this.el.find('li:first-child a'),
      dropTrigger = '<a class="drop-trigger" href="javascript: void(0)"><span>' + firstTab.text() + '</span></a>'

    this.el.append(tpl({ items, dropTrigger }));
  }

  _checkMobile() {
    this.isMobile = window.innerWidth <= 960;
  }

  _initListeners() {
    let $htmlOrBody = $('html, body');
    this.dropTrigger = this.el.find('.mobile-tabs-container > a');
    this.drop = this.el.find('.drop');
    this.mobileNav = this.el.find('.mobile-tabs');
    this.tabTriggers = this.el.find('a:not(.drop-trigger)');
    
    // Any link on a page may be targeting tabs
    $('body').on('click', 'a', (e) => {
      let link = e.currentTarget;
      if (this.tabTriggers.index(link) == -1) {
        if (link.hash && (link.hash in this.tabHashes)) {
          if (link.pathname === window.location.pathname) {
            this._openTabByHash(link.hash, true);
            window.location.hash = link.hash;
            e.preventDefault();
          }
        }
      }
    });

    this.dropTrigger.on('click', (el) => {
      if (this.mobileNav.hasClass(this.options.activeMobile)) {
        this._collapseDrop();
      } else {
        this._expandDrop();
      }
    });

    this.tabTriggers.on('click', (e, params) => {
      let clicked = $(e.currentTarget);
      this._collapseDrop();
      this._updateActiveTab(clicked);
      this._updateActiveContent(clicked);

      if (!params || !params.initialClick) {
        if (clicked.attr('href').length > 1) {
          window.location.hash = clicked.attr('href');
        }
        let target = $(this.el),
            stickyNavBefore = this._getPrevStickyNavHeight() || 0,
            topNumber = target.offset().top - stickyNavBefore;
        setTimeout(() => GLOBALS.ScrollTo.navigateTo(topNumber, 1000), 0);
      }
    
    });

    $(window).on("load resize", () => this._checkMobile());

    $htmlOrBody.on([
      'scroll',
      'mousedown',
      'wheel',
      'DOMMouseScroll',
      'mousewheel',
      'keyup',
      'touchmove'
    ].join(' '), function stopTabsAnimations() {
      $htmlOrBody.stop();
    });
  }

  _updateActiveTab(el) {
    let activeTab = $(el),
      index = this.isMobile ? this.linksMobile.index(activeTab) : this.links.index(activeTab);

    if (index != -1) {
      this.tabTriggers
        .parents('li')
        .removeClass(this.options.activeTabClass);
      $(this.links.parent('li').eq(index))
        .addClass(this.options.activeTabClass);
      $(this.linksMobile.parent('li').eq(index))
        .addClass(this.options.activeTabClass);
      this.dropTrigger.find('span').text(activeTab.text());
    }
  }

  _updateActiveContent(el) {
    let clickedLink = $(el),      
      index = this.isMobile ? this.linksMobile.index(clickedLink) : this.links.index(clickedLink);

    if (index != -1) {      
        this.contents
          .removeClass(this.options.activeClass)
          .eq(index)
          .addClass(this.options.activeClass)
          .trigger('tab.activated')
          .find('.slick-slider').slick('setPosition');

    }
  }

  _expandDrop() {
    $('body').on('click', this._clickOutsideCb);
    this.mobileNav.addClass(this.options.activeMobile);
    this.drop.slideDown(200);
  }

  _collapseDrop() {
    $('body').off('click', this._clickOutsideCb);
    this.mobileNav.removeClass(this.options.activeMobile);
    this.drop.slideUp(200);
  }

  _wrapSticky() {
    $(this.el).wrapInner("<div class='sticky-wrapper'></div>");
  }

  _initStickableBehaviour() {
    if (!this.el.hasClass('non-stickable')) {
      new Stickable(this.el.find(this.options.stickyWrapperSelector));
    }
  }

  _clickOutsideCb(e) {
    if (!$(e.target).closest('.molecule-lb-836').length) {
      this._collapseDrop();
    }
  }

  processContentsBackground() {
    this.contents.each((i, el) => {
      let $tabContent = $(el),
          sectionInSlider = $tabContent.find(this.options.sliderSelector),
          className = '';

      if (sectionInSlider.length && sectionInSlider[0].className.indexOf('bkg-color-') > -1) {
        sectionInSlider.each((i, el) => {
          let classes = el.className.split(' ');
          for (let i = 0; i < classes.length; i++) {
            if (classes[i].indexOf('bkg-color-') > -1) {
              className = classes[i];
            }
          }
        });
        $tabContent.find('.section').addClass(className);
      }
    });
  }

  _getPrevStickyNavHeight() {
    //flexContentParent is true when 836,830,835 are inside different .flex-content wrappers
    //sectionParent is true when 836,830,835 are inside one .flex-content and different .sections
    let prevParent,
      flexContentParent = this.el.parents(this.options.contentSelector).prevAll(this.options.contentSelector).not('.tab-content'),
      sectionParent = this.el.parents('.section').prevAll('.section');
     if (flexContentParent.length) {
        prevParent = flexContentParent;
     } else {
        prevParent = sectionParent;
     }
    let prevParentNear = prevParent.children().find(".molecule-lb-835, .molecule-lb-836, .molecule-lb-830").not(".non-stickable"),
      maxHeight = 0;

    if (prevParentNear.length > 0) {
      maxHeight = $(prevParentNear.eq(0)).height();
    }
    return maxHeight;
  }
}
