import { BaseComponent } from '../../components/base-component.js';


export class Molecule302 extends BaseComponent {

  static get selector () { return '.molecule-lb-302'; }

  get defaults() {
    return $.extend({}, super.defaults, {
      rowSelector: '.row',
      childrenSelector: '> *',
      imageSelector: 'img',
      imageHolderSelector: '.image-holder',
      contentWrapperSelector: '.content-wrapper',

      clonedClass: 'cloned',
      hiddenClass: 'hidden-container',
      noImageClass: 'no-image',
      noAllSpacingsClass: 'drop-all-spacings',
      stack: 'stack-ctas'
    });
  }

  constructor(...args) {
    super(...args);
    this._init();
  }

  _init() {
    let image = this.el.find(this.options.imageSelector).not('.dynamic-image');
    let contentWrapper = this.el.find(this.options.contentWrapperSelector);
    let hasAnyTextContent = contentWrapper.find(this.options.childrenSelector).length;

    if (image.length || this.el.find('.desktop').length) {
      (!this.el.find('.desktop').length) && this.el.find(this.options.imageHolderSelector).css({
        'background-image': `url("${image.attr('src')}")`
      }); 

      // NOTE: Append cloned copy only if there is an image together with content
      if (hasAnyTextContent) {
        contentWrapper.find(' > a.button, > .molecule-lb-406').wrapAll('<div class="cta-wrapper"></div>');
        if (this.el.hasClass(this.options.stack)) {
          this.el.find('.cta-wrapper > *:not(:last)').each((index, el) => $(el).after('<br />'));
        }
        let contentCopy = contentWrapper
            .clone(true, true)
            .addClass(`${this.options.clonedClass} ${this.options.hiddenClass}`);
            
        contentCopy.find('.overlay-popup').remove();
        this.el.append(contentCopy);
      }
    } else {
      this.el.addClass(this.options.noImageClass);
    }
    //in case if module has both classnames - there should not be noAllSpacingsClass on a ROW
    let preserveWidth = this.el.attr('class').match(/image-as-is|respect-image-metadata-width/g);
    if (!preserveWidth || (preserveWidth && preserveWidth.length < 2)) {
      this.el.parents(this.options.rowSelector).addClass(this.options.noAllSpacingsClass)
    }
  }
}
