import { BaseComponent } from '../../components/base-component.js';


export class Molecule823 extends BaseComponent {

  static get selector () { return '.molecule-823, .molecule-lb-823'; }

  /*get defaults() {
    return $.extend({}, super.defaults, {
      fixedColumnSelector: 'fixed-column'
    });
  }*/

  constructor(...args) {
    super(...args);
    this._init();
  }

  _init() {
    let cellsLength = this.el.find('.rows-wrapper .first_row > div').length;
    this.el.addClass('columns-' + cellsLength);
    /*if (this.el.hasClass(this.options.fixedColumnSelector)) {
    	this.el.closest('.section').addClass('table-columns-' + cellsLength);
    	this.el.find('>.rows-wrapper').stickyTableColumns({
    		columns: 2,
    		startFrom: 1
    	});
    }*/
  }
}
