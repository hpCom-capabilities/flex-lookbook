import { BaseComponent } from '../../components/base-component.js';


export class Molecule510 extends BaseComponent {

  static get selector () { return '.molecule-lb-510'; }

  constructor(...args) {
    super(...args);
    this._init();
  }

  _init() {
    this._setCollapsibleIndicator();
    this._updateHtml();
    this._initListeners();
  }

  _setCollapsibleIndicator() {
    if ($.trim(this.el.find('.description').text())) {
      this.el.find('.description').wrap('<div class="description-wrapper"/>');
      this.el.addClass('with-icon');
    } else {
      if (!this.el.find('.title *').length && this.el.is(':last-child')) {
        this.el.parent().addClass('ignore');
      }
    }
  }

  _updateHtml() {
    let src = this.el.find('img').attr('src'),
        imgWrapper = this.el.find('.img-wrapper');

    imgWrapper.css('background-image', 'url(' + src + ')');
    this.el.addClass('initialized');
  }

  _initListeners() {
    const parent = this.el.closest('.section.collage-layout');
    this.el.find('.expand-collapse-wrapper').on('click', () => {
      parent.find('.molecule-lb-510').not(this.el).each((index, el) => this._hideDescription($(el)));
      if (this.el.hasClass('active')) {
        this._hideDescription(this.el);
      } else {
        this._showDescription(this.el);
      }
    });
  }

  _hideDescription($el) {
    if ($el.hasClass('active')) {
      $el.find('.description').velocity('stop').velocity({opacity: 0}, {duration: 150});
      $el.removeClass('active').find('.description-wrapper').slideUp(300);
      this._enableMetrics($el);
    }
  }

  _showDescription($el) {
    $el.addClass('active');
    $el.find('.description').css('opacity', 0);
    $el.find('.description-wrapper').slideDown(300, () => {
      $el.find('.description').velocity('stop').velocity({opacity: 1});
    });
    this._disableMetrics($el);
  }

  _enableMetrics($el) {
    setTimeout(() => $el.find('.action-trigger').removeAttr('data-disable-metrics'));
  }

  _disableMetrics($el) {
    setTimeout(() => $el.find('.action-trigger').attr('data-disable-metrics', 'disabled'));
  }
}
