import { BaseComponent } from '../../components/base-component.js';


export class Molecule701 extends BaseComponent {

  static get selector () { return '.molecule-lb-701'; }

  get defaults() {
    return $.extend({}, super.defaults, {
      pageRowSelector: '.section',
      pageSectionSelector: '.flex-content',
      configElSelector: '.row-gradient-config'
    });
  }

  constructor(...args) {
    super(...args);
    this._init();
  }

  _init() {
    this._collectOptions();
    this._getBg();
    this._setBg();
  }

  _collectOptions() {
    this.configEl = this.el.find(this.options.configElSelector);
    this.options = $.extend({}, this.options, this.configEl.data());
  }

  _getBg() {
    this.options.bg = '';
    if (this.options.start) {
      this.options.bg += 'background: ' + this.options.start + ';';
      if (this.options.end) {
        let direction = (this.options.direction === 'vertical') ? 'bottom' : 'right';
        this.options.bg += 'background: linear-gradient(to ' + direction + ', ' + this.options.start + ' 0%,' + this.options.end + ' 100%);'
      }
    }
  }

  _setBg() {
    const applyTo = (this.options.area !== 'area-page-row') ? 'pageSection' : 'pageRow';
    const applyToEl = this.el.closest(this.options[applyTo + 'Selector']);
    this.options.bg && applyToEl.attr('style', this.options.bg);
  }
  
}
