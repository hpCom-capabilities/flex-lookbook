import { BaseComponent } from '../../components/base-component.js';


export class Molecule702 extends BaseComponent {

  static get selector () { return '.molecule-lb-702'; }

  constructor(...args) {
    super(...args);
    this._init();
  }

  _init() {
    if (!GLOBALS.Utils.device.isMobile) {
      this.videoWrapper = this.el.find('.ambient-video-wrapper');
      this.config = this.videoWrapper.find('.video-config').data();
      this.el.closest('.section').prepend(this.videoWrapper).addClass('ambient-video-section');
      setTimeout(() => this._createVideo());
    } else {
      this.el.remove();
    }
  }

  _createVideo() {
    this.video = document.createElement('video');
    this.config.mp4 && this._addSource(this.config.mp4, 'video/mp4');
    this.config.webm && this._addSource(this.config.webm, 'video/webm');
    this.video.setAttribute('autoplay', 'autoplay');
    this.video.setAttribute('muted', 'muted');
    this.video.setAttribute('loop', 'loop');
    this.videoWrapper.append(this.video);
    this.video.addEventListener('loadeddata', () => {
      this.el.closest('.section').addClass('video-loaded');
      this._initAspectRatio();
    });
  }

  _initAspectRatio() {
    this.ratio = this.video.videoWidth / this.video.videoHeight;
    $(window).on('resize', _.debounce(() => this._calcVideoWidth(), 100));
    this._calcVideoWidth();
  }

  _calcVideoWidth() {
    let minWidth = this.ratio * this.videoWrapper.height();
    $(this.video).css('min-width', minWidth);
  }

  _addSource(src, type) {
    const source = document.createElement('source');
    source.src = src;
    source.type = type;
    this.video.appendChild(source);
  }

  _playVideo() {
    this.video.play();
  }

}
