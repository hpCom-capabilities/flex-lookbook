import { runAsync } from '../helpers';
import { BaseComponent } from '../../components/base-component.js';


export class Molecule209 extends BaseComponent {

  static get selector () { return '.molecule-lb-209'; }

  get defaults() {
    return $.extend({}, super.defaults, {
      sectionSelector: '.section',
      wrapperSelector: '.molecule-ocmhpifootnotes__wrapper',
      footnotesSectionClass: 'molecule-ocmhpifootnotes-section',
      layoutWidth: 1280
    });
  }

  constructor(...args) {
    super(...args);
    this._init();
  }

  _init() {
    
    this._section = this.el.parents(this.options.sectionSelector);

    runAsync(() => {
      this._alignWithLayout();
      this._section.addClass(this.options.footnotesSectionClass);
    });
  }

  _alignWithLayout() {
      let maxWidth = this._getLayoutWidth(),
          wrapper = this.el.find(this.options.wrapperSelector);

      wrapper.css('max-width', maxWidth);
  }

  _getLayoutWidth() {
      let result = /layout-(.*?)(\D|$)/.exec(this._section.attr('class'));
      if (result) {
          return +result[1];
      }
      return this.options.layoutWidth;
  }

}
