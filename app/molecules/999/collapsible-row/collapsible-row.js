import { BaseComponent } from '../../../components/base-component.js';
import { Stickable } from '../../../common/scripts/stickable.js';


export class CollapsibleRows extends BaseComponent {

  static get selector () { return '.molecule-999 .collapsible-row'; }

  get defaults() {
    return $.extend({}, super.defaults, {
      collapseIconClass: '.collapsible-row-collapse-icon',
      lightThemeKeyword: 'appearance-theme-2',
      lightThemeClass: 'light-theme',
      secondaryThemeClass: 'secondary-theme'
    });
  }

  constructor(...args) {
    super(...args);
    this._init();
  }

  _init() {
    this.trigger = this.el.find('>a');
    this.triggerWrapper = this.trigger.closest('[class*="flex2-molecule"]')
                                      .addClass('collapsible-row-trigger-wrapper');

    this.triggerWrapper.closest('.row').css('line-height', '0');
    this._prepareCollapsibleRow();
    this._prepareCollapseIcon();
    this._initListeners();
  }

  _prepareCollapsibleRow() {
    let lastRowParent = this.el.parents('.row').last(),
        lightTheme = this.el.closest('.flex2-molecule').hasClass(this.options.lightThemeKeyword) ? this.options.lightThemeClass : '',
        secondaryTheme = this.triggerWrapper.hasClass(this.options.secondaryThemeClass) ? 'button-secondary-appearance' : '';

    //detect whether scructure is .section > .row + .section > .row OR .section > .row + .row;
    //condidition is truth for .section > .row + .row structure
    if (lastRowParent.next('.row').length) {
      this.row = lastRowParent.next('.row');
    } else {
      this.row = lastRowParent.parent().next('.section');
    }
    this.row.addClass('collapsible-row-container ' + lightTheme + ' ' + secondaryTheme).removeClass('hide-content');
  }

  _prepareCollapseIcon() {
    this.row.append(this._getCloseButton());
    this.collapseIcon = this.row.find(this.options.collapseIconClass);
  }

  _getCloseButton() {
    let closeButton = this.el.find('.collapsible-row-collapse-icon-wrap');
    if (!closeButton.length) {
      let wrapper = $('<div />', {'class': 'collapsible-row-collapse-icon-wrap'}),
          button = $('<a />', {
            'class': 'collapsible-row-collapse-icon',
            'href': 'javascript:void(0)',
            'data-metrics-title': 'show less',
            'data-metrics-link-type': 'link'
          });
      closeButton = wrapper.append(button);
    }
    this._setMetricsAttrs(closeButton);
    return closeButton;
  }

  _setMetricsAttrs(closeButton) {
    const parentModule = this.el.parent('.flex2-molecule'),
          dataTitle = parentModule.attr('data-metrics-title'),
          dataIdentifier = parentModule.attr('data-metrics-identifier');

    dataTitle && closeButton.attr('data-metrics-title', dataTitle);
    dataIdentifier && closeButton.attr('data-metrics-identifier', dataIdentifier);
    closeButton.addClass('track-all-links flex2-molecule');
    parentModule.addClass('track-all-links');
  }

  _initListeners() {

    this.trigger.on('click', () => {
      this._expandRow();
    });

    this.collapseIcon.on('click', () => {
      this.collapseIcon.removeClass('initialized');
      this._collapseRow();
    });
  }

  _expandRow() {
    this.triggerWrapperHeight = this.triggerWrapper.outerHeight(true);

    this.row.on('transitionend webkitTransitionEnd', (e) => {
       if (e.originalEvent.propertyName == 'max-height' && $(e.target).is(this.row)) {
        this.row.off('transitionend webkitTransitionEnd');
        this.row.css('max-height', 'none');
       }
    });

    this.row.css('max-height', this.row[0].scrollHeight);

    this.triggerWrapper.on('transitionend webkitTransitionEnd', (e) => {
      if (e.originalEvent.propertyName == 'margin-top' && $(e.target).hasClass('collapsible-row-trigger-wrapper')) {
        this.triggerWrapper.off('transitionend webkitTransitionEnd');
        this.triggerWrapper.addClass('disabled');
        this.stickableCollapseBtn = new Stickable(this.collapseIcon, null, {type: 'bounding'});
        this.collapseIcon.addClass('initialized');
      }
    });

    this.triggerWrapper.addClass('inactive').css({'margin-top': -this.triggerWrapperHeight + 'px'});
    GLOBALS.ScrollTo.navigate(null, null, 1000, this.row, -this.triggerWrapperHeight);
  }

  _collapseRow() {
    setTimeout(() => this.stickableCollapseBtn.destroy(), 300);
    let navigateOffset = this.row.offset().top - $(window).scrollTop() + this.triggerWrapperHeight - 300;

    this.triggerWrapper.on('transitionend webkitTransitionEnd', (e) => {
      if (e.originalEvent.propertyName == 'margin-top' && $(e.target).hasClass('collapsible-row-trigger-wrapper')) {
        this.triggerWrapper.off('transitionend webkitTransitionEnd');
        this.triggerWrapper.removeClass('inactive disabled');
      }
    });
    this.row.css('max-height', this.row.height())
    setTimeout(() => {
      this.row.css('max-height', 0);
      this.triggerWrapper.css('margin-top', 0);
    });

    GLOBALS.ScrollTo.navigateToOffset(navigateOffset, 1000, null, true);
  }

}
