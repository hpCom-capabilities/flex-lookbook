import { BaseComponent } from '../../../components/base-component.js';
import { Stickable } from '../../../common/scripts/stickable.js';

export class ScrollToTop extends BaseComponent {

  static get selector () { return '.molecule-999 a.scroll-to-top'; }

  get defaults() {
    return {
      offsetTop: 500,
      scrollDuration: 600
    }
  }
  constructor(...args) {
    super(...args);
    this._init();
  }

  _init() {
    new Stickable(this.el, null, {type: 'bottom', offsetTop: +this.el.attr('data-show-offset') || this.options.offsetTop});
    this._initListeners();
  }

  _initListeners() {
    this.el.on('click', () => GLOBALS.ScrollTo.navigateTo(0, +this.el.attr('data-scroll-duration') || this.options.scrollDuration));
  }
}
