import { BaseComponent } from '../../../components/base-component.js';

export class InlineVideo extends BaseComponent {

  static get selector () { return '.molecule-999 .video-module'; }

  get defaults() {
    return {
      offsetTop: 100,
      loop: false,
      offsetBottom: 100,
      playInViewportClassname: 'play-once-in-viewport',
      loopInViwportClassname: 'loop-in-viwport',
      replayIconSelector: '.replay-video',
      playIconTpl: '<a class="play js_overlay_trigger" href="javascript:void(0)" title="play video" data-metrics-tcmid="245-1909760" data-metrics-tags="asset," data-metrics-link-type="" data-metrics-title="Video w/o related videos at the end"></a>'
    }
  }
  constructor(...args) {
    super(...args);
    this.mobile = GLOBALS.Utils.device.isMobile;
    this._init();
  }

  _init() {
    this.video = this.el.find('video').length && this.el.find('video')[0];
    if (this.video) {
      this.parentModule = this.el.closest('.flex2-molecule');
      this.replayIcon = this.el.find(this.options.replayIconSelector);

      /* for mobiles we need to show play icon with poster image cause autoplay
       * is not working and we need to trigger video to play manually*/
      if (this.mobile) {
        this.replayIcon.after(this.options.playIconTpl);
        this.playIcon = this.el.find('a.play');
      } else {
        this.playInViewport = this.el.hasClass(this.options.playInViewportClassname);
        this.loopInViewport = this.el.hasClass(this.options.loopInViwportClassname);
      }

      //Ignore playing in a viewport for mobile devices
      if (!this.mobile && this.playInViewport) {
        this.outOfViewport = true;

        this.viewportParent = this.el.closest('.slick-slider');
        this.viewportParent = this.viewportParent.length ? this.viewportParent : this.el;
        this.viewportParent.viewportChecker({
          repeat: true,
          offset: 100,
          callbackFunction: (obj, e) => {
            if (e === 'add') {
              if (this.outOfViewport) {
                this._playVideo();
                this.outOfViewport = false;
              }
            } else {
              this._pauseVideo();
              this.outOfViewport = true;
            }
          }
        });
      }
      //play video in active slide only for desktops
      if (!this.mobile) {
        this.parentModule.on('slider.viewportin', () => setTimeout(() => this._playVideo(), 100));
        //pause video in inactive slide for both desktops and mobiles
        this.parentModule.on('slider.viewportout', () => setTimeout(() => this._pauseVideo(), 100));
      } else {
        this.parentModule.on('slider.viewportout', () => setTimeout(() => {
          if (this.video.isPlaying) {
            this._showPlayIcon();
          }
          this._pauseVideo();
        }, 100));
      }

      this._initVideoEvents();
    }
  }

  _playVideo() {
    if (!this.mobile && this.playInViewport) {
      if (this.viewportParent.hasClass('visible')) {
        if (!this.video.isPlaying) {
          if (this.el.closest('.slick-slider').length) {
            if (this.el.closest('.slick-active').length) {
              setTimeout(() => this._play(), 50);
            }
          } else {
            setTimeout(() => this._play(), 50);
          }

        }
      }
    } else {
      if (!this.video.isPlaying) {
        this._play();
      }
    }
  }

  _play() {
    this._hideReplayIcon();
    this._hidePlayIcon();
    this.video.play();
  }

  _pauseVideo() {
    if (this.video.isPlaying) {
      this.video.pause();
    }
  }

  _initVideoEvents() {

    if (!this.mobile) {
      $(this.video)
        .on('loadedmetadata', () => {
          this.video.currentTime = 0.01;
        })
        .on('ended', () => {
          if (!this.loopInViewport) {
            this._showReplayIcon();
          } else {
            this._playVideo();
          }
        });
    } else {
      this._showPlayIcon();
      $(this.video).on('ended', () => this._showReplayIcon());
    }

    this.replayIcon && this.replayIcon.on('click', (e) => {
      if (!this.video.isPlaying) {
        this._playVideo();
      }
    });

    this.playIcon && this.playIcon.on('click', (e) => {
      this._playVideo();
    });
  }

  _showPlayIcon() {
    this.playIcon && this.playIcon.addClass('visible');
  }

  _hidePlayIcon() {
    this.playIcon && this.playIcon.removeClass('visible');
  }

  _showReplayIcon() {
    this.replayIcon && this.replayIcon.addClass('visible');
  }

  _hideReplayIcon() {
    this.replayIcon && this.replayIcon.removeClass('visible');
  }

}
