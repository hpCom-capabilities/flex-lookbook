import {BaseComponent} from '../../../components/base-component.js';

export class ScrollToNext extends BaseComponent {

  static get selector() {
    return '.molecule-999 a.scroll-to-next';
  }

  get defaults() {
    return {
      scrollDuration: 1000,
      scrollToSectionClassName: 'scroll-to-section-arrow',
    }
  }

  constructor(...args) {
    super(...args);
    this._init();
  }

  _init() {
    this.$section = this.el.closest('.section');
    this.$wrapper = this.el.closest('.molecule-999');
    if (this.$section.length) {
      this.$section.addClass(this.options.scrollToSectionClassName);
      this._prepareSection();
    }
    this.scrollDuration = this.el.attr('data-scroll-speed') || this.options.scrollDuration;
    this._initListeners();
  }

  _prepareSection() {
    if (this.$wrapper.hasClass('show-above-1280')){
      this.$section.addClass('arrow-above-1280');
    }
    if (this.$wrapper.hasClass('show-below-1280')){
      this.$section.addClass('arrow-below-1280');
    }
    if (this.$wrapper.hasClass('show-above-720')){
      this.$section.addClass('arrow-above-720');
    }
    if (this.$wrapper.hasClass('show-below-720')){
      this.$section.addClass('arrow-below-720');
    }
  }

  _initListeners() {
    this.el.on('click', (e) => {
      let offset = $('.molecule-lb-835:not(.non-stickable)').outerHeight() || $('.molecule-lb-830:not(.non-stickable)').outerHeight() || 0;
      GLOBALS.ScrollTo.navigateToSection($(e.currentTarget).attr('href'), null, +this.scrollDuration, null, -offset);
      e.preventDefault();
    });
  }
}
