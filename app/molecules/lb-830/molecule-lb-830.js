import { BaseComponent } from '../../components/base-component.js';
import { Stickable } from '../../common/scripts/stickable.js';


export class Molecule830nonr extends BaseComponent {

  static get selector() { return '.molecule-lb-830'; }

  get defaults() {
    return $.extend({}, super.defaults, {
      disableStickableClassname: 'non-stickable',
      swapAtBreakpoint: '720',
      closeAllOnClick: '.level2 >li:not(.selectable) a, .level2 > .selectable .expand-area a, .level3-container a',
      mobileQuery: window.matchMedia('screen and (max-width: 720px)')
    });
  }

  constructor(...args) {
    super(...args);
    this._init();
  }

  _init() {
    this.nav = this.el.find('nav');

    if (this._hasSubmenu()) {
      this._prepareContent();
      this.dropTrigger = this.el.find('.title a');
      this._initListeners();
    } else {
      this.el.addClass('no-submenu');
    }

    if (!this.el.hasClass(this.options.disableStickableClassname)) {
      this._initStickableBehaviour();
    }
  }

  _hasSubmenu() {
    return !!this.el.find('ul.level2').length;
  }

  _prepareContent() {
    let copyContent = '';
    let wrapNav = $('<div />', { class: 'nav-wrapper' });

    this.level3Container = $('<div />', { class: 'level3-container' });
    this.el.find('.level2 > .selectable > .expand-area').each((i, item) => {
      let el = $(item),
        id = el.attr('id');
      copyContent += '<div id="' + id + '-desktop" class="expand-area">' + el.html() + '</div>';
    });
    this.level3Container.html(copyContent);
    wrapNav.append(this.el.find('.level2'));
    wrapNav.find('.level2').wrap('<div class="level1-container"/>');
    wrapNav.append(this.level3Container);
    this.nav.append(wrapNav);

    this.drop = wrapNav;
    this.allDesktopDrops = wrapNav.find('.level3-container .expand-area');
    this.mobileDrops = wrapNav.find('.selectable .expand-area');
    this.allTriggers = wrapNav.find('.level2 >li.selectable > a');
    this.level2Container = this.el.find('.level2');
  }

  _initListeners() {
    this.el.find('.level2 > .selectable > a').on('click', (e) => {
      let el = $(e.currentTarget),
        preffix = this._getPreffix(),
        relId = $(e.currentTarget).attr('rel') + preffix,
        drop = $('#' + relId);

      if (el.hasClass('active')) {
        this.allDesktopDrops.removeClass('active');
        this.mobileDrops.slideUp();
        this.allTriggers.removeClass('active');
        this.level2Container.removeClass('active');
      } else {
        //preffix truthy only for desktops
        if (preffix) {
          this.allDesktopDrops.removeClass('active');
          this.level2Container.addClass('active');
          drop.addClass('active');
        } else {
          this.mobileDrops.slideUp();
          drop.slideDown();
        }
        this.allTriggers.removeClass('active');
        el.addClass('active');
      }
    });
    this.dropTrigger.on('click', (e) => {
      let el = $(e.currentTarget);
      e.preventDefault();
      if (el.hasClass('active')) {
        this._closeAll();
      } else {
        el.addClass('active');
        this.drop.slideDown(() => {
          this.drop.css('display', 'flex');
        });

        this._addCustomListeners();
      }
    });

    //click on any direct link should close dropdown
    this.el.find(this.options.closeAllOnClick).on('click', (e) => this._closeAll());
    this.options.mobileQuery.addListener((e) => this._closeAll(e));
  }

  _getPreffix() {
    let preffix = window.matchMedia('screen and (min-width: ' + this.options.swapAtBreakpoint + 'px)').matches ? '-desktop' : '';
    return preffix;
  }

  _initStickableBehaviour() {
    this.stickableEl = this.el.find('nav');
    new Stickable(this.stickableEl, this.stickableEl.parent());
  }

  _closeAll() {
    this.mobileDrops.slideUp();
    this.drop.slideUp(() => {
      this.allTriggers.removeClass('active');
      this.allDesktopDrops.removeClass('active');
      this.level2Container.removeClass('active');
    });

    this.dropTrigger.removeClass('active');
    this._removeCustomListeners();
  }

  _clickOutsideCb(e) {
    if (!$(e.target).closest('.molecule-lb-830').length) {
      this._closeAll();
    }
  }

  _addCustomListeners() {
    $('body').on('click.nav830', (e) => this._clickOutsideCb(e));
    $(window).on('scroll.nav830', _.debounce(() => this._closeAll(), 50));
  }

  _removeCustomListeners() {
    $('body').off('click.nav830');
    $(window).off('scroll.nav830');
  }
}
