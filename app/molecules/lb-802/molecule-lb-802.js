import { BaseComponent } from '../../components/base-component.js';


export class Molecule802 extends BaseComponent {

  static get selector () { return '.molecule-lb-802'; }

  get defaults() {
    return $.extend({}, super.defaults, {
      stack: 'stack-ctas'
    });
  }

  constructor(...args) {
    super(...args);
    this._init();
  }

  _init() {
    this.clickAllowed = true;
    this.mediaMobile = window.matchMedia('screen and (max-width: 720px)');
    this._initContentSlider();
    this._initTriggers();
    this._initListeners();
  }

  _initContentSlider() {
    this.contents = this.el.find('.contents .js-tab-content');
    this.contentSlider = this.el.find('.contents').addClass('grid-slider no-controls transition-fade disable-manual-control adaptive-height');
    this.contentSlider
      .on('init', (e, slick) => this.contentSliderSlick = slick)
      .on('beforeChange', (...args) => this.clickAllowed = false)
      .on('afterChange', (e, slick) => this.clickAllowed = true);
  }

  _initListeners() {
    
    this.triggers.on('click', (e) => {
      if (this.clickAllowed) {
        let index = $(e.currentTarget).index();
        this.contentSliderSlick.slickGoTo(index);
        this._activateTrigger($(e.currentTarget));
      }
    });

    this.triggersMover.on('transitionend webkitTransitionEnd', () => this._afterActivate());

    this.el.closest('.flex-content').on('tab.activated', _.debounce(() => this._activateTrigger(this.currentTrigger, true), 30));
    //wait until fonts are loaded to properly get the width of trigger link
    $(window).on('resize', _.debounce(() => this._activateTrigger(this.currentTrigger, true), 30));

    //show mover only after load event to avoid extra movements
    $(window).on('load', _.debounce(() => {
      this._checkCenterMode();
      this._activateTrigger(this.currentTrigger, true);
      this._initCenterMode();
    }, 30));

    window.matchMedia('screen and (max-width: 720px)').addListener((event) => this._checkCenterMode());
  }

  _initTriggers() {
    this.triggersWrapper = this.el.find('.triggers');
    this.triggersNav = this.triggersWrapper.find('nav');
    this.triggers = this.triggersWrapper.find('a');
    this.triggersMover = this.triggersWrapper.find('.triggers-mover > span');
    this._checkCenterMode();
    this._activateTrigger(this.triggers.eq(0));
  }

  _activateTrigger(el, centerTrigger) {
    let offsetLeft = el.find('>span').position().left,
        width = el.find('>span').width();

    this.triggersMover.css({
      'width': width,
      'transform': `translateX(${offsetLeft}px)`
    });
    this.currentTrigger = el;
    centerTrigger && this._centerActive(200);
  }

  _afterActivate() {
    this._areTriggersScrollable() && this._centerActive(200);
  }

  _centerActive(duration) {
    let width = this.currentTrigger.find('>span').width(),
        offsetLeft = this.currentTrigger.find('>span').position().left,
        toOffset = this.triggersWrapper.width() / 2 - width / 2;

    duration = duration || 0;
    //NOTE, velocity doesn't work ok here
    this.triggersWrapper.stop().animate({
      scrollLeft: offsetLeft - toOffset
    }, duration);
  }

  _checkCenterMode() {
    let leftOffset = 0,
        rightOffset = 0,
        firstTrigger = this.triggers.first(),
        lastTrigger = this.triggers.last();
        

    if (this.mediaMobile.matches && this._areTriggersScrollable()) {
      leftOffset = (this.triggersWrapper.width() - firstTrigger.find('span').width()) / 2;
      rightOffset = (this.triggersWrapper.width() - lastTrigger.last().find('span').width()) / 2;
    }
    firstTrigger.css({marginLeft: leftOffset});
    lastTrigger.css({marginRight: rightOffset});
  }

  _areTriggersScrollable() {
    let width = this.triggersWrapper.width(),
        scrollWidth = this.triggersWrapper.get(0).scrollWidth;

    return scrollWidth > width;
  }

  _initCenterMode() {
    this._centerActive();
    this.triggersMover.addClass('ready');
  }

}
