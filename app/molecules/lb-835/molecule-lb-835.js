import { BaseComponent } from '../../components/base-component.js';
import { Stickable } from '../../common/scripts/stickable.js';


export class Molecule835 extends BaseComponent {

  static get selector () { return '.molecule-lb-835'; }

  get defaults() {
    return $.extend({}, super.defaults, {
      rowSelector: '.section',
      alternativeThemeClass: 'appearance-theme-2',
      alternativeThemeRowClass: 'main-nav-835-theme-2',
      rowClass: 'main-nav-835',
      scrollSpeed: 1000,
      mobileTpl: 
        '<div class="mobile-nav">'+
          '<div class="anchors">'+
            '<%= dropTrigger %>'+
            '<div class="drop"><ul><%= items %></ul></div>'+
          '</div>'+
          '<% if (btn) { %>'+
            '<div class="btn-wrapper small-btn">'+
              '<%= btn %>'+
            '</div>'+
          '<% } %>'+
        '</div>'
    });
  }

  constructor(...args) {
    super(...args);
    this._init();
  }

  _init() {
    if (this.el.hasClass(this.options.alternativeThemeClass)) {
      this.parentSection = this.el.closest(this.options.rowSelector).addClass(this.options.alternativeThemeRowClass);
      this._activateLink();
      $(this.el).wrapInner("<div class='inner-wrapper'></div>")
      if (!this.el.hasClass('non-stickable')) {
        this._initStickableBehaviour();
        this._initHeightUpdate();
      }
      return;
    }
    let length = this.el.find('.links > li').length;
    this.el.addClass('width-' + length);
    this.el.closest(this.options.rowSelector).addClass(this.options.rowClass);
    this.scrollSpeed = this.el.attr('data-scroll-speed') || this.options.scrollSpeed;
    this._markExternalLinks();
    this._prepareBuyButton();
    this._prepareMobile();
    this._initListeners();
    if (!this.el.hasClass('non-stickable')) {
      this._initStickableBehaviour();
    }
    this.activeNavIndex = 0;
    this._clickOutsideCb = this._clickOutsideCb.bind(this);
  }

  _activateLink() {
    var currentLocation = window.location.href;
    this.el.find('.links > li > a').each((index, el) => {
      if ($(el).attr('href') === currentLocation) {
        $(el).addClass('active');
      }
    });
  }

  _markExternalLinks() {
    this.el.find('.links > li > a').each((index, el) => {
      if ($(el).attr('href').indexOf("#") !== 0) {
        $(el).addClass('external-page-link');        
      }
    });
  }

  _prepareBuyButton() {
    let lastLink = this.el.find('.links > li:last-child > a'),
        drop = this.el.find('.molecule-lb-406');

    if (lastLink.length && lastLink.attr('href').indexOf('#') !== 0) {
      lastLink.addClass('button primary').closest('li').addClass('small-btn');
    }
    if (drop.length) {
      drop.parent().addClass('drop-link');
    }
  }

  _prepareMobile() {
    let tpl = _.template(this.options.mobileTpl),
        items = this.el.find('.links').clone().html(),
        btn = this.el.find('li.small-btn, li.drop-link').clone().html(),
        firstAnchor = this.el.find('.links > li:first-child > a'),
        dropTrigger = '<a class="drop-trigger" href="javascript: void(0)">' + firstAnchor.text() + '</a>'

    this.el.append(tpl({items, btn, dropTrigger}));
  }

  _initListeners() {
    this.dropTrigger = this.el.find('.anchors > a');
    this.drop = this.el.find('.drop');
    this.mobileNav = this.el.find('.mobile-nav');
    this.anchorTriggers = this.el.find('.links > li > a:not(.button, .external-page-link), .mobile-nav .drop > ul > li > a:not(.button, .external-page-link)');
    this.anchorSections = this._collectSections();
    
    this.dropTrigger.on('click', (el) => {
      if (this.mobileNav.hasClass('active')) {
        this._collapseDrop();
      } else {
        this._expandDrop();
      }
    });

    this.anchorTriggers.on('click', (e) => {
      this.dropTrigger.html($(e.currentTarget).text());
      this._collapseDrop();
      GLOBALS.ScrollTo.navigate($(e.currentTarget).attr('href'), this._updateActiveAnchor.bind(this), +this.scrollSpeed);
      e.preventDefault();
    });

    $(window).on('scroll', () => {
      if (!GLOBALS.ScrollTo.animating) {
        this._updateActiveAnchor();
      }
    });
    this.anchorSections.forEach((section) => {
      section.on('activated-by-scroll-to', () => this._updateActiveAnchor());
    });
  }

  _updateActiveAnchor() {
    let currentNavHeight = this.el.outerHeight(),
        activeSection;
    for (let i = this.anchorSections.length - 1; i >= 0; i--) {
      let clientRect = this.anchorSections[i][0].getBoundingClientRect();

      if (clientRect.top < currentNavHeight) {
        if ( i !== this.anchorSections.length - 1 ) {
          activeSection = this.anchorSections[i];
          break;
        } else {
          if (clientRect.bottom > currentNavHeight) {
            activeSection = this.anchorSections[i];
          }
          break;
        }
      };
    }
    if ( activeSection ) {
      let activeSectionAnchor = activeSection.data('anchor'),
          activeTriggers = this.anchorTriggers.filter((index, el) => {
            return $(el).attr('href') === '#' + activeSectionAnchor;
          });
      this.anchorTriggers.removeClass('active');
      this.dropTrigger.html(activeTriggers.eq(0).text());
      activeTriggers.addClass('active');
    } else {
      this.anchorTriggers.removeClass('active');
    }
  }

  _expandDrop() {
    $('body').on('click', this._clickOutsideCb);
    this.mobileNav.addClass('active');
    this.drop.slideDown(200);
  }

  _collapseDrop() {
    $('body').off('click', this._clickOutsideCb);
    this.mobileNav.removeClass('active');
    this.drop.slideUp(200);
  }

  _initStickableBehaviour() {
    new Stickable(this.el);
  }

  _initHeightUpdate() {
    const row = this.parentSection.find('>.row');
    const updateHeight = () => {
      row.height(this.el.height());
    }
    $(window).on('load resize', _.debounce(() => updateHeight(), 100));
    updateHeight();
  }

  _clickOutsideCb(e) {
    if (!$(e.target).closest('.molecule-lb-835').length) {
      this._collapseDrop();
    }
  }

  _collectSections() {
    let sections = [];
    this.el.find('.links > li > a:not(.button, .external-page-link)').each((index, el) => {
      let anchorId = $(el).attr('href').replace('#', ''),
          anchorSeaction = $('[data-anchor="' + anchorId + '"]');
      anchorSeaction.length && sections.push(anchorSeaction);
    });
    return sections;
  }
  
}
