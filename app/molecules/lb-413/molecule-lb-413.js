import { BaseComponent } from '../../components/base-component.js';

export class Molecule413 extends BaseComponent {

  static get selector () { return '.molecule-lb-413'; }

  get defaults() {
    return $.extend({}, super.defaults, {
      customAppearanceClassname: 'appearance-theme-2'
    });
  }

  constructor(...args) {
    super(...args);
    this._init();
  }

  _init() {
    if (this.el.hasClass(this.options.customAppearanceClassname)) {
      this._initCustomAppearance();
    } else {
      this._wrapImage();
    }
  }

  _initCustomAppearance() {
    let img = this.el.find('img'),
      height = +this.el.find('img').attr('height'),
      background = 'url(' + img.attr('src') + ')';
    img.parent().css({
      'background-image': background,
      height: height
    });

    this._setInitialized();
  }

  _wrapImage() {
    this.el.find('img').each((i, el) => $(el).wrap('<div />'));
  }

  _setInitialized() {
    this.el.addClass('initialized');
  }

}
