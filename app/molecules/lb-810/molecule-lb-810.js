import { runAsync } from '../helpers';
import { BaseComponent } from '../../components/base-component';
import { BrightcovePlayer } from '../../components/video/brightcove-player/brightcove-player';
import { YoutubePlayer } from '../../components/video/youtube-player/youtube-player';


export class Molecule810 extends BaseComponent {

  static get selector () { return '.molecule-lb-810'; }

  get defaults() {
    return $.extend({}, super.defaults, {
      youtubeVideoSelector: '.youtube-video-template',
      brightcoveVideoSelector: '.bc-video-template',
    });
  }

  constructor(...args) {
    super(...args);
    //release DOM during video init
    runAsync(() => this._init());
  }

  _init() {
    this.player = this._createVideoPlayer();
    this.player.init(() => console.log('video is initialized'));
  }

  _createVideoPlayer() {
    let youtubeVideoElement = this.el.find(this.options.youtubeVideoSelector);
    let brightcoveVideoElement = this.el.find(this.options.brightcoveVideoSelector);

    if (youtubeVideoElement.length) {
      return new YoutubePlayer(youtubeVideoElement, {playsInline: true});
    } else if (brightcoveVideoElement.length) {
      return new BrightcovePlayer(brightcoveVideoElement, {playsInline: true});
    }
  }
}
