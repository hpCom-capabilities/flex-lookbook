import { BaseComponent } from '../../components/base-component.js';


export class Molecule501 extends BaseComponent {

  static get selector() { return '.molecule-lb-501'; }

  get defaults() {
    return $.extend({}, super.defaults, {
      textOverImageClassname: 'text-over-image',
      stack: 'stack-ctas',
      alignCTAs: '.align-cta-bottom',
      equalCells: '.equal-cells',
      alignDescriptions: '.align-descriptions'
    });
  }

  constructor(...args) {
    super(...args);
    this._init();
  }

  _init(el) {

    this._updateCTAs();
    this._resolveInlineBGColor();
    this._handleTextOverImage();
    this._handleStackCTAs();
    this._initListeners();
  }

  _updateCTAs() {
    let linkLists = this.el.find('.link-lists').length ? '.link-lists' : '';
    this.shouldAlignCTAs = this.el.closest(this.options.alignCTAs).length;
    this.el.find(linkLists + ' > a.button,' + linkLists + ' > .molecule-lb-406').wrapAll('<div class="cta-wrapper"></div>');
  }

  /**
   * If equal-cells is in place and module 501 has inline background - transmit this color
   * to the closest CELL, in order to be able to add some top and bottom spacings
   */
  _resolveInlineBGColor() {
    if (this.el.closest(this.options.equalCells).length && this.el[0].style.backgroundColor) {
      this.el.parent().css('background', this.el[0].style.backgroundColor);
    }
  }

  _handleTextOverImage() {
    if (this.el.hasClass(this.options.textOverImageClassname)) {
      this.el.find('> *:not(.videoitem):not(.clf)').wrapAll('<div class="module-content"></div>');
    }
  }

  _handleStackCTAs() {
    if (this.el.hasClass(this.options.stack) || this.shouldAlignCTAs) {
      this.el.find('.cta-wrapper > *:not(:last)').each((index, el) => $(el).after('<br />'));
    }
  }

  _initListeners() {
    //if align-descriptions KW is set up for the parent ROW - listen to equal cells beforeUpdate event
    //to update heights of titles prior to that change
    if (this.el.closest(this.options.alignDescriptions).length) {
      this.el.on('equalheight', (e, data) => {
        let mols = data.cells.find('.molecule-lb-501'),
            maxHeight = 0;

        mols.find('.font-lh3').css('min-height', 0);
        mols.each((i, el) => {
          let title = $(el).find('.font-lh3');
          if (title.length) {
            (title.height() > maxHeight) && (maxHeight = title.height());
          } else {
            $(el).find('.description').before($('<h3 />', {'class': 'font-lh3'}));
          }
        });
        mols.find('.font-lh3').css('min-height', maxHeight);
      });

      this.el.on('equalheightauto', (e, data) => {
        data.cells.find('.molecule-lb-501 .font-lh3').css('min-height', 0);
      });
    }
  }
}
