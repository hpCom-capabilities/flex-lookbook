import { BaseComponent } from '../../components/base-component.js';


export class Molecule300 extends BaseComponent {

  static get selector () { return '.molecule-lb-300'; }

  get defaults() {
    return $.extend({}, super.defaults, {
      stack: 'stack-ctas'
    });
  }

  constructor(...args) {
    super(...args);
    this._init();
  }

  _init() {
    this.el.find(' > a.button, > .molecule-lb-406').wrapAll('<div class="cta-wrapper"></div>');
    if (this.el.hasClass(this.options.stack)) {
      this.el.find('.cta-wrapper > *:not(:last)').each((index, el) => $(el).after('<br />'));
    };
    this.el.addClass('initialized');
  }
}
