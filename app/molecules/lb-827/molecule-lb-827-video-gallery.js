import { BaseComponent } from '../../components/base-component';
import { YoutubePlayer } from '../../components/video/youtube-player/youtube-player';
import { BrightcovePlayer } from '../../components/video/brightcove-player/brightcove-player';
import { LbSlider } from '../../components/sliders/lb-slider';

export class VideoGallery extends BaseComponent {

  // This class is added automatically to this.el component
  get ROOT_CLASS() { return 'video-gallery' }

  get defaults() {
    return $.extend({}, super.defaults, {
      enableArrowsMobileClass: 'enable-arrows-mobile',
      playIconTpl: () => '<a class="play js_overlay_trigger" href="javascript:void(0)" title="play video" data-metrics-link-type="link" data-metrics-title="Play video"></a>'
    });
  }

  constructor(...args) {
    super(...args);
    this._posters = [];
    this._players = [];
    this._videoGallery = null;
    this._init();
  }

  _init() {
    //lazy init when in content-overlay
    if (this.el.closest('.content-overlay').length) {
      this._lazyInit();
    } else {
      this._instantInit();
    }
  }

  _lazyInit() {
    this.el.one('overlay.opened', () => this._instantInit());
    //get rid of extra grid spacings
    this.el.one('overlay.beforeopen', () => this.el.closest('.content-overlay').find('>.section').addClass('drop-all-spacings'));
    this.el.on('overlay.closed', () => this._refreshGallery());
  }

  _instantInit() {
    this._contentItems = this.el.find('.content-list .item');
    this._prepareHTML();
    this._initVideoItems();
    this._buildSliders();
  }

  _prepareHTML() {
    this.faderHelperEl = $('<div />', {class: 'fader-helper'});
    this.el.find('.content-wrapper').prepend(this.faderHelperEl);
  }

  _initVideoItems() {
    this._videoGallery = new VideoItemsFactory(this._contentItems);
  }

  _buildSliders() {
    this._mainSlider = this.el.find('.content-list');
    this._thumbSlider = this.el.find('.thumbnails-list').addClass('default');

    this._mainSlider
      .on('init', (e, slick) => this._onInit(e, slick))
      .on('beforeChange', (...args) => this._beforeChange(...args))
      .on('afterChange', (e, slick) => this._afterChange(e, slick))
      .slick({
        touchMove: false,
        infinite: false,
        touchThreshold: 100,
        swipe: false,
        fade: true
      });

    let mainSliderSlick = this._mainSlider.slick('getSlick');

    //init thumbs slider
    this._thumbsLbSlider = new LbSlider(this._thumbSlider, {
      visibleSlides: 3,
      onSlide: mainSliderSlick.slickGoTo.bind(mainSliderSlick),
      customClass: 'small-arrows',
      alignArrowsToEl: 'a > img',
      infinity: false,
      waitForTrue: () => !mainSliderSlick.animating,
      responsive: [{
        bp: 720,
        visibleSlides: 2.5
      }]
    });

  }

  _refreshGallery() {
    //pause all videos and show posters
    this._videoGallery.pauseAll(true);

    //activate first slide
    this._mainSlider.slick('slickGoTo', 0, true);
  }

  _onInit(e, slick) {
    //update position & size on initialization
    setTimeout(() => slick.setPosition(), 100);

    //position arrows relatively to image
    GLOBALS.SliderHelper.alignArrowsToEl(this._mainSlider, {alignToElSelector: '.video-container'});

    //align black helper element's height with a height of videos
    GLOBALS.SliderHelper.alignElementsHeight(this._mainSlider, this._mainSlider.find('.slick-current .video-container'), this.faderHelperEl);

    //show arrows for mobiles
    this._mainSlider.find('.slick-arrow').addClass(this.options.enableArrowsMobileClass + ' animate-top');

    //set counter
    this._mainSlider.append(`<span class="pagingInfo" class="pagingInfo">1/${slick.slideCount}</span>`);
    this._counter = this._mainSlider.find('.pagingInfo');

    //counter placement is updated after each setPosition call
    this._mainSlider.on('setPosition', () => this._updateCounterPlacement());
  }

  //TODO handle arrows properly for a new slider
  _onInitThumbsSlider(e, slick) {
    GLOBALS.SliderHelper.alignArrowsToEl(this._thumbSlider, {alignToElSelector: 'a > img'});
    this._thumbSlider.find('.slick-arrow').addClass(this.options.enableArrowsMobileClass + ' animate-top');
  }

  _beforeChange(e, slick, prevIndex, nextIndex) {
    this._videoGallery.pauseAll();
    this._thumbsLbSlider.slideTo(nextIndex, true);
    this._updateCounter(slick.slideCount, nextIndex);
  }

  _afterChange(e, slick) {
    this._videoGallery.showPosters(slick.currentSlide);
  }

  _updateCounter(slideCount, currentSlide) {
    this._counter.text(`${currentSlide + 1}/${slideCount}`);
  }

  _updateCounterPlacement() {
    let newTop = this._mainSlider.find('.slick-current .video-container').first().height();

    newTop && this._counter.css('top', newTop);
  }

}

class VideoItemsFactory {
  constructor(items) {
    this._init(items);
  }

  _init(items) {
    this._items = items;
    this._videos = [];
    this._lazyInit(0);
  }

  _lazyInit(index) {
    let el = this._items.eq(index);
    if (el.length) {
      this._videos.push(new VideoItem(el));
      setTimeout(() => this._lazyInit(++index), 500);
    }
  }

  pauseAll(showPoster) {
    this._videos.forEach((video) => video.pauseVideo(showPoster));
  }

  showPosters(excludeIndex) {
    this._videos.forEach((video, i) => (i !== excludeIndex) && video.showPoster());
  }

}

class VideoItem {
  get options() {
    return {
      youtubeVideoSelector: '.youtube-video-template',
      brightcoveVideoSelector: '.bc-video-template',
      mobileQuery: window.matchMedia('screen and (max-width: 960px)'),
      playIconTpl: () => '<a class="play js_overlay_trigger" href="javascript:void(0)" title="play video" data-metrics-link-type="link" data-metrics-title="Play video"></a>'
    }
  }

  constructor(el) {
    this._init(el);
  }

  _init(el) {
    this.el = el;
    this._poster = null;
    this._player = null;
    this._initPoster();
    this._initPlayer();
    this._initListeners();
  }

  _initPoster() {
    let desktopBg = this.el.find('.desktop').attr('data-src'),
        mobileBg = this.el.find('.mobile').attr('data-src') || desktopBg,
        posterEl = $('<div />', {
          'class': 'img',
          'style': this._getBgStyle(desktopBg, mobileBg)
        });

    this._poster = this.el.find('.image-container')
                          .append(posterEl)
                          .append(this.options.playIconTpl())
                          .data({
                            bgs: [desktopBg, mobileBg]
                          });
  }

  _updatePosterImg() {
    this._poster.find('.img').attr('style', this._getBgStyle(...this._poster.data().bgs));
  }

  _initPlayer(el) {
    this._player = this._createVideoPlayer();
    this._player.init();
  }

  _initListeners() {
    this.el.find('.play').on('click', () => this.playVideo());
    this.options.mobileQuery.addListener(() => this._updatePosterImg());
  }

  _createVideoPlayer() {
    let youtubeVideoElement = this.el.find(this.options.youtubeVideoSelector),
        brightcoveVideoElement = this.el.find(this.options.brightcoveVideoSelector);

    if (youtubeVideoElement.length) {
      return new YoutubePlayer(youtubeVideoElement);
    } else if (brightcoveVideoElement.length) {
      return new BrightcovePlayer(brightcoveVideoElement);
    }
    return {};
  }

  _getBgStyle(imgUrlDesktop, imgUrlMobile) {
    let imgUrl = this.options.mobileQuery.matches ? imgUrlMobile : imgUrlDesktop
    return `background: url(${imgUrl}) 50% 50% / cover no-repeat scroll;`;
  }

  _hidePoster() {
    this._poster.fadeOut(800);
  }

  showPoster() {
    this._poster.fadeIn(400);
  }

  playVideo() {
    this._player.play();
    this._hidePoster();
  }

  /*
   * @param {boolean} showPoster - indicates whether to show poster after video has been paused
   *                               and rewinds video to the beginning
   */
  pauseVideo(showPoster) {
    if (this._player) {
      if (!showPoster) {
        this._player.pause();
      } else {
        this._player.stop();
        this.showPoster();
      }
    }

  }

}