import { BaseComponent } from '../../components/base-component.js';
import { VideoGallery } from './molecule-lb-827-video-gallery.js';
import { ImageGallery } from './molecule-lb-827-image-gallery.js';


export class Molecule827Factory extends BaseComponent {

  static get selector () { return '.molecule-lb-827'; }

  get defaults() {
    return $.extend({}, super.defaults, {
      videoGalleryIndicator: '.video-container'
    });
  }

  constructor(...args) {
    super(...args);
    this._init();
  }

  _init() {
    if (this.el.find(this.options.videoGalleryIndicator).length) {
      new VideoGallery(this.el);
    } else {
      new ImageGallery(this.el);
    }
  }
}