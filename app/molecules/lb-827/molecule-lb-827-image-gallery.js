import { runAsync } from '../helpers';
import { BaseComponent } from '../../components/base-component.js';

export class ImageGallery extends BaseComponent {

  // This class is added automatically to this.el component
  get ROOT_CLASS() { return 'image-gallery' }

  get defaults() {
    return $.extend({}, super.defaults, {
      enableArrowsMobileClass: 'enable-arrows-mobile',
      carouselCounterClass: 'display-carousel-counter',
      amountSlides: 0,
      fullScreenIconUrl: '/us/en/images/i/lookbook/icon_fullscreen.png',
      fullScreenCloseIconUrl: '/us/en/images/i/lookbook/icon_fullscreen_close.png'
    });
  }

  constructor(...args) {
    super(...args);
    this._init();
  }

  _init() {
    //lazy init when in content-overlay
    if (this.el.closest('.content-overlay').length) {
      this._lazyInit();
    } else {
      this._instantInit();
    }
  }

  _lazyInit() {
    this.el.one('overlay.opened', () => this._instantInit());
    //get rid of extra grid spacings
    this.el.one('overlay.beforeopen', () => this.el.closest('.content-overlay').find('>.section').addClass('drop-all-spacings'));
  }

  _instantInit() {
    this.switcherEl = null;
    this.counterEl = null;
    this.faderHelperEl = null;
    this.fullScreenModeOpened = false;
    this.fullScreenIcon = $('<a class="fullscreen-mode-icon">' +
      '<img class="fullscreen-open" src=' + this.options.fullScreenIconUrl + '>' +
      '<img class="fullscreen-close" src=' + this.options.fullScreenCloseIconUrl + '></a>');
    this.thumbnailList = this.el.find('.thumbnails-list');
    this.imageContainer = this.el.find('.image-container');
    this.gallerySlider = this.el.find('.content-list');
    this.initialMedia = this._checkMedia();
    this._setImages();
    this._renderImages();
    this._wrapForScroll();
    this._prepareHTML();
    this.initListeners();
    this._addSwitcher();
    this._buildSlider();
    this._setCounter();
    this.onReady();
  }

  initListeners() {

    this.thumbnailList.find('.item').on('mouseenter', (e) => {
      this._scrollTo($(e.currentTarget));
    });

    this.thumbnailList.find('.item').on('click', (e) => {
      let el = $(e.currentTarget);

      this.thumbnailList.find('.item').removeClass("active");
      el.addClass("active");
      //console.log(el);
      this.gallerySlider.slick('slickGoTo', el.index());
    });

    this.gallerySlider.on('init reInit', (e, slick, currentSlide) => this._setActive(slick.currentSlide));
    this.gallerySlider.on('beforeChange', (e, slick, currentSlide, nextSlide) => this._setActive(nextSlide));

    this._initFullScreenListeners();
  }

  _prepareHTML() {
    if (this.el.parents('.content-overlay').length) {
      this.faderHelperEl = $('<div />', {class: 'fader-helper'});
      this.el.find('.content-wrapper').prepend(this.faderHelperEl);
    }
    this._embedFullScreenIcon();
    this._embedFullScreenCloseIcon();
  }

  _scrollTo(tab) {
    let scrollingPanel = this.el.find('.scrolling-panel');
    let scrollingEl = this.el.find('.thumbnails-list');

      if (scrollingPanel.width() < scrollingPanel.get(0).scrollWidth) {
        let elPosLeft = Math.floor(tab.position().left),
            posLeft = elPosLeft - parseInt(this.thumbnailList.css("padding-left"));

        scrollingEl.velocity('stop').velocity('scroll', {
          container: scrollingPanel,
          axis: 'x',
          offset: posLeft,
          duration: 300
        });
      }
  }

  _checkMedia() {
    var media;
    if (window.innerWidth > 720) {
      media = "desktop";
    } else {
      media = "mobile";
    }
    return media;
  }

  _crossBreakpoint() {
    let media = this._checkMedia();
    if (media == this.initialMedia) {
      return;
    } else {
      this.initialMedia = media;
      this._renderImages();
    }
  }

  _setImages() {
    this.imageContainer.each((index, el) => {
      let elem = $(el),
        container = elem.find("div"),
        options = {};

      container.each(function(index, it) {
        let item = $(it);
        if (item.hasClass("desktop")) {
          options.srcDesktop = item.data("src");
        } else if (item.hasClass("mobile")) {
          options.srcMobile = item.data("src");
        }
        options.alt = item.data("alt") || "";
        item.remove();
      });

      elem.append(this._createImage(options));
    });
  }

  _renderImages() {
    this.imageContainer.find('img').each((index, image) => {
      let imageItem = $(image),
        desktopSrc = imageItem.data("src-desktop"),
        mobileSrc = imageItem.data("src-mobile"),
        src = "";
      if (window.innerWidth > 720) {
        src = desktopSrc;
      } else {
        src = mobileSrc ? mobileSrc : desktopSrc;
      }

      imageItem.attr("src", src);
    });

  }

  _createImage({ srcDesktop, srcMobile, alt }) {
    let image = $('<img />', {
      alt: alt,
      "data-src-desktop": srcDesktop,
      "data-src-mobile": srcMobile,
      class: srcMobile ? "hasMobileImage" : ""
    });
    return image;
  }

  _wrapForScroll() {
    let panel = $('<div />', {
      class: 'scrolling-panel'
    });
    this.thumbnailList.parents('.thumbnails-wrapper').wrapInner(panel);
  }

  _buildSlider() {
    this.gallerySlider
    .on('init', (e, slick) => this._onInit(e, slick))
    .slick({
      touchMove: false,
      infinite: false,
      touchThreshold: 20,
      initialSlide: 0,
      fade: true
    });
    this.el.find('.slick-arrow').addClass(this.options.enableArrowsMobileClass + ' animate-top');
  }

  _onInit(e, slick) {
    if(this.faderHelperEl) {
      GLOBALS.SliderHelper.alignElementsHeight(this.gallerySlider, this.gallerySlider.find('.slick-current .image-container'), this.faderHelperEl);
    }
    //position arrows relatively to image
    GLOBALS.SliderHelper.alignArrowsToEl(this.gallerySlider, {alignToElSelector: '.image-container'});
  }

  _setCounter() {
    if (this.el.hasClass(this.options.carouselCounterClass)) {
      let slick = this.gallerySlider.slick('getSlick');
      this.options.amountSlides = slick.slideCount;
      this.counterEl = $('<span />', {class: 'pagingInfo'});
      this.gallerySlider.on('reInit beforeChange', (e, slick, currentSlide, nextSlide) => this._updateCounter(nextSlide))
             .append(this.counterEl);

      this._updateCounter(slick.currentSlide);
      this.gallerySlider.on('setPosition', () => this._updatePlacement(this.counterEl));
    };
  }

   _updateCounter(currentSlide) {
    this.counterEl.text(`${currentSlide + 1}/${this.options.amountSlides}`);
  }

  _updatePlacement(el) {
    let newTop = this.gallerySlider.find('.slick-current .image-container').first().height();

    newTop && el.css('top', newTop);
  }

  _setActive(slide) {
    this.thumbnailList.find('.item').removeClass("active");
    var activeTab = $(this.thumbnailList.find('.item').eq(slide)).addClass('active');

    this._scrollTo(activeTab);
    setTimeout(() => this._updateUnderline(activeTab), 100);
  }

  _updateUnderline(activeTab) {
    var active = activeTab ? activeTab : this.thumbnailList.find('.item.active'),
      position,
      marginType;
      if(GLOBALS.Utils.isRTL()) {
        marginType = "margin-right";
        position = this.thumbnailList.width() - active.position().left - active.width() + (this.thumbnailList.outerWidth() - this.thumbnailList.width()) / 2 + "px";
      } else {
        marginType = "margin-left";
        position = active.position().left - (this.thumbnailList.outerWidth() - this.thumbnailList.width()) / 2 + "px";
      }
    this.switcherEl.css("width", active.width()).css(marginType, position);
  }

  _addSwitcher() {
    this.switcherEl = $('<div/>', {class: 'switcher-inner'});
    let switcherWrapper = $('<div />', {class: 'switcher-wrapper'}).append(this.switcherEl);
    this.thumbnailList.append(switcherWrapper);
  }

  onReady() {
    this.gallerySlider.slick('setPosition');

    $(window).on('resize', _.debounce(() => {
      this._crossBreakpoint();
      this.gallerySlider.slick('setPosition');
      this._updateUnderline();
    }, 300));
  }

  /* START: FullScreen Mode functionality */

  _embedFullScreenIcon() {
    this.el.find('.content-wrapper').append(this.fullScreenIcon);
    this.gallerySlider.on('setPosition', () => this._updatePlacement(this.fullScreenIcon));
  }

  _embedFullScreenCloseIcon() {
    let overlayEl = this.el.closest('.content-overlay');
    if (overlayEl.length) {
      let fullScreenClose = overlayEl.find('.popup-close').clone(true);
      fullScreenClose
        .on('click', () => {
          this._goOutFullScreen();
          this._forceCloseFullScreen();
        })
        .appendTo(this.el);
    }
  }

  _initFullScreenListeners() {
    $(document).on('fullscreenchange webkitfullscreenchange mozfullscreenchange MSFullscreenChange', () => {
      if (!this._isFullScreenCurrently() && this.fullScreenModeOpened) {
        this._forceCloseFullScreen();
      }
    });

    this.el.find('.fullscreen-mode-icon').on('click', (e) => {
      let activeImageGallery = $(e.currentTarget).closest('.image-gallery').get(0);
      if (this.fullScreenModeOpened) {
        this._goOutFullScreen();
      } else {
        this._goInFullScreen(activeImageGallery);
      }
      this.fullScreenModeOpened = !this.fullScreenModeOpened;
      let fullScreenModeIcon = $(e.currentTarget).closest('.fullscreen-mode-icon');
      fullScreenModeIcon.toggleClass('opened', this.fullScreenModeOpened);
    })
  }

  _forceCloseFullScreen() {
    this.fullScreenModeOpened = false;
    $(this.el).find('.fullscreen-mode-icon').removeClass('opened');
  }

  _goInFullScreen(el) {
    if (el.requestFullscreen)
      el.requestFullscreen();
    else if (el.mozRequestFullScreen)
      el.mozRequestFullScreen();
    else if (el.webkitRequestFullscreen)
      el.webkitRequestFullscreen();
    else if (el.msRequestFullscreen)
      el.msRequestFullscreen();
  }

  _goOutFullScreen() {
    if (document.exitFullscreen)
      document.exitFullscreen();
    else if (document.mozCancelFullScreen)
      document.mozCancelFullScreen();
    else if (document.webkitExitFullscreen)
      document.webkitExitFullscreen();
    else if (document.msExitFullscreen)
      document.msExitFullscreen();
  }

  _isFullScreenCurrently() {
    let full_screen_element = document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement || null;
    return !(full_screen_element === null);
  }

  /* END: FullScreen Mode functionality */

}
