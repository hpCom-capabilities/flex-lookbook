import { BaseComponent } from '../../components/base-component.js';

export class Molecule406 extends BaseComponent {

  static get selector() { return '.molecule-lb-406'; }

  get defaults() {
    return $.extend({}, super.defaults, {
      moduleSelector: '.molecule-lb-406',
      dropSelector: '.dropdown-menu',
      centerModuleClassname: 'align-module-center',
      alignRightClassname: 'align-module-right',
      triggerSelector: '.dropdown-toggle',
      clickableHelperSelector: '.clickable',
      ctaAppearanceClassname: 'enable-cta-appearance'
    });
  }

  constructor(...args) {
    super(...args);
    this._init();
  }

  _init() {
    this._handleAlignment();
    this._initListeners();
    this.closeTimeout = null;
  }

  _handleAlignment() {
    const clickableEl = $('<div />', { class: 'clickable' });
    this.el.find(this.options.triggerSelector).before(clickableEl);
    if (this.el.hasClass(this.options.centerModuleClassname)) {
      this.el.parent('[class*="span"]').addClass('text-center');
    }
    if (this.el.hasClass(this.options.alignRightClassname)) {
      this.el.parent('[class*="span"]').addClass('text-right');
    }

    //some config of 406 makes it opaque by default and waits until JS will make some updates
    this.el.addClass('initialized');
  }

  _initListeners() {
    this.el.find(this.options.clickableHelperSelector).on('click', (e) => {
      $(this.options.moduleSelector).not(this.el).removeClass('active');
      if (this.el.hasClass('active')) {
        this.el.removeClass('active');
        this.el.removeClass('outside-left-edge outside-right-edge');
      } else {
        this.el.addClass('active');
        this._initCloseOnBodyCLick();
        this._checkOutsideScreen();
        /**
         * when dd has class this.options.ctaAppearanceClassname and only one option in a list -
         * just click on it when dd is clicked
         */
        if (this.el.hasClass(this.options.ctaAppearanceClassname)) {
          if (this.el.find(this.options.dropSelector + ' > li').length === 1) {
            this.el.find(this.options.dropSelector + ' a:first')[0].click();
          }
        }
      }


    });
    this.el.on('focusout', () => {
      this.closeTimeout = setTimeout(() => {
        this.el.removeClass('active');
      }, 400);
    });
    this.el.find(this.options.dropSelector).on('click', 'a', () => setTimeout(() => this.el.removeClass('active'), 0));
    $(window).on("resize", _.debounce(() => this._checkOutsideScreen(), 500));
  }
  _initCloseOnBodyCLick() {
    $('body').off('click.dd').on('click.dd', (e) => {
      if (!$(e.target).closest(this.options.moduleSelector).length) {
        $(this.options.moduleSelector).removeClass('active');
        $('body').off('click.dd');
        this.el.removeClass('outside-left-edge outside-right-edge');
        clearTimeout(this.closeTimeout);
      }
    });
  }

  _checkOutsideScreen() {

    this.el.removeClass('outside-left-edge outside-right-edge');

    let dropWidth = $(this.el).find(this.options.dropSelector).outerWidth(),
      dropLeft = $(this.el).find(this.options.dropSelector).offset().left,
      dropRight = $(window).width() - dropLeft - dropWidth;

    if (dropLeft < 0) {
      this.el.addClass('outside-left-edge');
    }

    if (dropRight <= 0) {
      this.el.addClass('outside-right-edge');
    }
  }
}
