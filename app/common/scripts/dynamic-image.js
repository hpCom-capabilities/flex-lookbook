export class DynamicImage {

  static isConstructor() {return true;} 

  get media() {
    return {
      tablet: window.matchMedia('screen and (max-width: 1279px)'),
      mobile: window.matchMedia('screen and (max-width: 496px)')
    }
  }

  constructor(script, options) {
    this.el = script.closest('.flex2-molecule');
    this.options = options;
    //prevent any acident second evaluation (eg by sliders and others)
    script.remove();

    this._init();
  }

  _init() {
    (this.options.module == 'lb-703') && this._initRowImageFlip();
    (this.options.module == 'lb-302') && this._init302ImageFlip();
    
    this.data = {
      desktop: this.el.find('.desktop').data(),
      tablet: this.el.find('.tablet').data() || this.el.find('.desktop').data(),
      mobile: this.el.find('.mobile').data() || this.el.find('.tablet').data() || this.el.find('.desktop').data()
    }
    if (this.data.desktop) {
      this._update();
      this._initListeners();
    }
  }

  _init302ImageFlip() {
    this.row = this.el.find('.image-holder');
    if (this.el.hasClass('image-as-is')) {
      this.image = $('<img />');
      if (this.row.find('.mbot15').length) {
        this.row.find('.mbot15').append(this.image);
      } else {
        this.row.find('div[data-src]').last().after(this.image);
      }
    }
  }

  _initRowImageFlip() {
    this.row = this.el.closest('.section').addClass('dynamic-bg');
  }

  _getCurrentMedia() {
    let platform = 'desktop';
    if (this.media.tablet.matches) {
      platform = this.media.mobile.matches ? 'mobile' : 'tablet';
    }
    return platform;
  }

  _updateBg(platform) {
    this.row.css('background-image', 'url(' + this.data[platform].src + ')');
  }

  _updateHeight(platform) {
    this.row.css('height', this.data[platform].height);
  }

  _updateWidth(platform) {
    this.row.css('width', this.data[platform].width);
  }

  _updateImageMeta(platform) {
    this.image.attr({
      src: this.data[platform].src,
      width: this.data[platform].width,
      height: this.data[platform].height,
      alt: this.data[platform].alt
    });
  }

  _update() {
    let platform = this._getCurrentMedia();
    let sliderParent = this.el.closest('.slick-slider');
    this._updateBg(platform);
    (this.options.module == 'lb-703') && this._updateHeight(platform);
    (this.options.module == 'lb-302') && this._updateWidth(platform);
    this.image && this._updateImageMeta(platform);
    
    sliderParent.length && setTimeout(() => sliderParent[0].slick.refresh(), 20);
  }

  _initListeners() {
    this.media.tablet.addListener(() => this._update());
    this.media.mobile.addListener(() => this._update());
  }
}