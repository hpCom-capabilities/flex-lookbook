'use strict';

export class Loader {

  get options() {
    return {
      scrollMagic: 'https://hpcom-capabilities.bitbucket.io/lookbook/static/js/ScrollMagic.min.js'
    };
  }

  _loadJsFile(fileName) {
    if (!this[fileName]) {
      this[fileName] = new Promise((resolve, reject) => {
        this._embedScript(this.options[fileName], resolve, reject)
      });
    }
    return this[fileName];
  }

  _embedScript(url, onLoad, onError) {
    let script = document.createElement('script');
    script.onload = onLoad;
    script.onerror = onError;
    script.src = url;
    document.body.appendChild(script);
  }
}
