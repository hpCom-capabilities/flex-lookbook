'use strict';

export class SliderHelper {

  /**
   * @param {jquery DOM el} slider - slider to work with
   * @param {object} options - configuration
   * @description - alignes position of arrow to the middle of a certain item within a slider
   *                Item is specified in options.alignToElSelector. Item is a single DOM el from 
   *                first slide for all slides
   */
  alignArrowsToEl(slider, options) {
    let arrows = options.arrows ? options.arrows.css('margin-top', 0) : slider.find('.slick-arrow').css('margin-top', 0),
        alignToEl = options.alignToEl || slider.find('.slick-current ' + options.alignToElSelector).first();
    
    function _align() {
      let arrowHeight = arrows.first().height(),
          alignToElHeight = alignToEl.height();

      alignToElHeight && arrows.css('top', (alignToElHeight - arrowHeight) / 2);
    }
    _align();

    slider.on('setPosition', _.debounce(() => _align(), 50));
  }


  /**
   * @param {jquery DOM el} takeHeightFromEl - element, which height should be used to apply to the other el
   * @param {jquery DOM el} applyHeightToEl - element, that should accept a new height from takeHeightFromEl
   * @description - alignes heights of multiple elements {applyHeightToEls}, based on {takeHeightFromEl} height
   */
  alignElementsHeight(slider, takeHeightFromEl, applyHeightToEls) {
    if (!takeHeightFromEl || !applyHeightToEls) return;
    function _align() {
      let height = takeHeightFromEl.height();

      applyHeightToEls.height(height);
    }
    _align();

    slider.on('setPosition', _.debounce(() => _align(), 50));
  }

}