'use strict';

export class SegmentedContent {

  get options() {
    return {
      segmentsSelector: '.section[class*="segment"], .flex2-molecule[class*="segment"]',
      queryParamName: 'jumpId',
      segmentActiveClassname: 'activated-segment',
      segmentFromQuery: {
        'pageSegment1': 'segment-1',
        'pageSegment2': 'segment-2',
        'pageSegment3': 'segment-3',
        'pageSegment4': 'segment-4',
      }
    }
  }
  constructor(...args) {
    $(() => this._init());
  }

  _init() {
    this.segments = $(this.options.segmentsSelector);
    this.segments.length && $(() => this._initSegments());
  }

  _initSegments() {
    //dummy for fisibility
    this._generateMediaTaxSegment();
    this.currentSegment = this._getSegmentFromJumpId() || this.dummyMediaTax;
    
    $(`.${this.currentSegment}`).addClass(this.options.segmentActiveClassname);
  }
  _getSegmentFromJumpId() {
    let query = GLOBALS.Utils._parseQuery(window.location.search);
    return this.options.segmentFromQuery[query[this.options.queryParamName]];
  }

  _generateMediaTaxSegment() {
    let segments = Object.values(this.options.segmentFromQuery);
    this.dummyMediaTax = segments[this._getRandomInt(0, 4)]
    $('#mediaTaxSegment').text(this.dummyMediaTax);
  }

  _getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
  }

}