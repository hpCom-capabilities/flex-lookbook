'use strict';

export class Stickable {

  static isConstructor() {return true;} 
  
  get options() {
    return {
      type: 'regular', // regular|bounding. Defines type of skickable behaviour
      stickyClass: 'sticky',
      offsetTop: 120,
      offsetBottom: 0
    }
  }

  constructor(stickableEl, staticParent, opt) {
    this.opts = $.extend({}, this.options, opt);
    this.el = stickableEl;
    this.staticParent = staticParent || stickableEl.closest('.section');
    this._init();
  }
  
  _init() {
    this.calculate = () => this._calculate()[this.opts.type]();
    this.calculate();

    $(window).on('scroll', this.calculate);
    this._destroy = () => $(window).off('scroll', this.calculate);
  }

  _calculate() {
    return {
      'regular': () => {
        let top = this.staticParent.offset().top;
        this.el[($(window).scrollTop() + 1) > top ? 'addClass' : 'removeClass'](this.opts.stickyClass);
      },
      'bounding': () => {
        let clientRect = this.staticParent[0].getBoundingClientRect(),
          wHeight = $(window).height();
        this.el[((clientRect.top + this.opts.offsetTop) < wHeight && (clientRect.bottom - this.opts.offsetBottom) > wHeight) ? 'addClass' : 'removeClass'](this.opts.stickyClass);
      },
      'bottom': () => {
        let scrollTop = $(window).scrollTop();
        this.el[scrollTop > this.opts.offsetTop ? 'addClass' : 'removeClass'](this.opts.stickyClass);
      }
    }
  }

  destroy() {
    this.el.removeClass(this.opts.stickyClass);
    this._destroy();
  }
}
