'use strict';
import { BaseComponent } from '../../components/base-component';

export class RegularMetrics extends BaseComponent {
  get defaults() {
    return $.extend({}, super.defaults, {
      metricEvent: "click",
      trackPrefix: "2.0",
      trackName: "new.link",
      metaToCollect: [
        { name: "bu" },
        { name: "sub_bu" },
        { name: "simple_title" }
      ],
      attrsToCollect: [
        { name: "identifier" },
        { name: "title" }
      ]
    });
  }

  constructor(...args) {
    super(...args);
    $(window).on('load', () => {
      if (!this.isWelcomeHome()) {
        this._init();
      }
    });
    //$(() => this._init());
  }

  isWelcomeHome() {
    return window.hpmmd && hpmmd.page.name.indexOf('welcome-home') != -1;
  }

  _init() {
    let meta = $("meta"),
      metaValue = '',
      selector = '.flex2-molecule[data-metrics-identifier] ' +
      'a[href]:not([href*="slideshare"], [href="#"], [href*="javascript:void(0)"]), ' +
      '.flex2-molecule[data-metrics-identifier] a.js_overlay_trigger, ' +
      '.flex2-molecule[data-metrics-identifier] a.action-trigger, ' +
      '.flex2-molecule[data-metrics-identifier] a.iframe-popup-trigger, ' +
      '.js_menu_holder a[href]:not([href="#"], [href*="javascript:void(0)"]),' +
      'a[data-metrics-identifier], .track-all-links a:not([onclick])';
    this.languageCode = document.documentElement.lang.split("-")[0]; //ja||us
    this.countryCode = document.documentElement.lang.split("-")[1]; //jp||en
    this.metaValues = [];
    this.links = [];
    this.timer = null;

    for (var q = 0; q < this.options.metaToCollect.length; q++) {
      metaValue = meta.filter("*[name='" + this.options.metaToCollect[q].name + "']").attr("content");
      if (metaValue) {
        if (this.options.metaToCollect[q].name == 'simple_title') {
          if (metaValue.length > 40) {
            metaValue = metaValue.substr(0, 40);
          }
        }
        this.metaValues.push(metaValue);
      }

      if (this.options.metaToCollect[q].name == 'sub_bu') {
        this.metaValues.push(this.countryCode);
        this.metaValues.push(this.languageCode);
      }

    }
    $(document).on('click', selector, (e) => {
      this.delayedTrackCall(e);
    });
    $(document).on('mousedown touchstart', '.metalocator-buy-wrapper', (e) => {
      this.delayedTrackCall(e);
    });

    $(document).on('sendMetric', function(e) {
      _self.sendMetric(e.$link);
    });
  }

  delayedTrackCall(e) {
    if (!this.timer) {
      this.timer = setTimeout(() => {
        clearTimeout(this.timer);
        this.timer = null;
      }, 100);
      if (e.which !== 3) {
        this.sendMetric($(e.currentTarget));
      }
    }
  }

  sendMetric(elementToTrack) {
    let metricTargetProps = [],
      metricsType = '',
      metricValue = "",
      props = this.options.attrsToCollect;

    metricTargetProps = this.collectTrackingValue(elementToTrack, props);
    metricValue = (this.options.trackPrefix + "/" + this.metaValues.join("/") + "/" + metricTargetProps);
    metricsType = elementToTrack.attr('data-metrics-link-type') || '';

    //handle collapse/expand links, ignore sending metrics for expanded state
    if (elementToTrack.attr('data-disable-metrics')) {
      return;
    }

    //videos & popups
    if (elementToTrack.hasClass('js_overlay_trigger') || elementToTrack.hasClass('iframe-popup-trigger')) {
      metricsType = 'link';
    }

    if (metricsType) {
      try {
        console.log('trackMetrics:' + this.options.trackName + ', name: ' + metricValue + ', type: ' + metricsType);
        trackMetrics(this.options.trackName, { name: metricValue, type: metricsType });
      } catch (excpt) {
        console.log(excpt.message);
      };
    } else {
      console.log('trackMetrics has NOT beet called cause data-metrics-link-type param is not set to a link');
    }

    return true;
  }

  collectTrackingValue(target, props) {

    let valueArray = [],
      value = "";

    if (target.length === 0) {
      return value;
    }

    for (let i = 0; i < props.length; i++) {
      let metricCnt = '';

      if (props[i].name == 'identifier') {
        if (target.parents('.flex2-molecule[data-metrics-identifier]').length) {
          metricCnt = target.parents('.flex2-molecule[data-metrics-identifier]').attr('data-metrics-identifier');
        } else {
          if (target.attr('data-metrics-identifier') !== 'undefined') {
            metricCnt = target.attr('data-metrics-identifier');
          } else {
            metricCnt = '';
          }
        }
      }

      if (props[i].name == 'title') {

        if (target.parents('.flex2-molecule').length) {
          metricCnt = target.parents('.flex2-molecule').attr("data-metrics-" + props[i].name)
        } else if (target.parents('.js_menu_holder').length) {
          metricCnt = target.parents('.js_menu_holder').attr("data-metrics-" + props[i].name);
        }

        let targetTitle = target.attr("data-metrics-" + props[i].name);

        if (targetTitle) {
          if (target.hasClass('js_overlay_trigger') || target.hasClass('iframe-popup-trigger')) {
            if (targetTitle.length > 20) {
              targetTitle = targetTitle.substr(0, 20);
            }
          }
          if (metricCnt) {
            metricCnt = metricCnt + '/' + targetTitle;
          } else {
            metricCnt = targetTitle;
          }
        }

      }

      if (metricCnt) {
        valueArray.push(metricCnt);
      }

    }

    if (valueArray.length) {
      value = valueArray.join('/');
    }

    return value;
  }
}


export class HomepageMetrics extends BaseComponent {

  get defaults() {
    return $.extend({}, super.defaults, {
      metricEvent: "click",
      promoImpressionEventType: "promoClosedImpression",
      attrsToCollect: [
        { name: "position" },
        { name: "title" }
      ],
      ccLLCodeValues: [],
      links: [],
      languageCode: '',
      countryCode: '',
      selector: "body #content .body"
    });
  }

  constructor(...args) {
    super(...args);
    $(window).on('load', () => {
      if (this.isWelcomeHome()) {
        this._init();
      }
    });
  }

  indexBanners() {
    var banners = $('body #content .body').find('.flex-content');
    $.each(banners, function(index, ele) {
      $(ele).attr('data-metrics-position', 's' + (index + 1));
    });
  }

  isWelcomeHome() {
    return window.hpmmd && hpmmd.page.name.indexOf('welcome-home') != -1;
  }

  _init() {
    this.options.languageCode = document.documentElement.lang.split("-")[0]; //ja||us
    this.options.countryCode = document.documentElement.lang.split("-")[1]; //jp||en
    this.options.ccLLCodeValues.push(this.options.countryCode);
    this.options.ccLLCodeValues.push(this.options.languageCode);
    this.options.links = this.gatherLinks($(this.options.selector));
    this.indexBanners();
    // Call after indexBanners
    this.sendBannerImpressionMetric($(this.options.selector));
    $.each(this.options.links, (index, ele) => {
      $(ele).on(this.options.metricEvent, (e) => {
        this.sendLinkMetric($(e.currentTarget));
      });
    });

  }

  // gather links which are inside molecule and have attribute 'data-metrics-link-type'
  gatherLinks(element) {
    var linksArr = element.find('.flex-content .flex2-molecule a[data-metrics-link-type]');
    return linksArr;
  }

  // gather banners which have links with attribute 'data-metrics-link-type'
  // and don't have parent section with class 'no-impression'
  gatherBanners(element) {
    var BannerArr = [];
    element.find('.flex-content').filter(function(i, ele) {
      var list = $(ele).find('.flex2-molecule a[data-metrics-link-type]');
      for (var j = 0; j < list.length; j++) {
        var x = $(list[j]).parents('div.section').hasClass('no-impression');
        if (!x) {
          BannerArr.push($(ele));
          break;
        }
      }
    });
    return BannerArr;
  }

  // banners only
  sendBannerImpressionMetric(element) {
    var bannerArr = this.gatherBanners(element),
      messages = [],
      props = this.options.attrsToCollect,
      impressionMetricValue,
      self = this;
    $(bannerArr).each(function(index, ele) {
      var bannerMetricsMsg = self.impressionTrackingValue($(ele), props),
        impressionMetricValue = (self.options.ccLLCodeValues.join("_") + "_" + bannerMetricsMsg);
      messages.push(impressionMetricValue);
    });
    console.log("********Lookbook Homepage :Banner Impression metrics triggered:", this.options.promoImpressionEventType + ":" + messages);
    trackMetrics(this.options.promoImpressionEventType, { 'messages': messages });
  }

  // links only
  sendLinkMetric(elementToTrack) {
    var metricTargetProps = [],
      metricLinkType = '',
      metricValue = "";
    var props = this.options.attrsToCollect;
    metricTargetProps = this.linkTrackingValue(elementToTrack, props);
    metricValue = (this.options.ccLLCodeValues.join("_") + "_" + metricTargetProps);
    metricLinkType = elementToTrack.attr('data-metrics-link-type') || '';
    if (metricLinkType != "") {
      try {
        if (metricLinkType == "promo") {
          console.log("********Lookbook Homepage : promoClick Event triggered:", metricValue);
          trackMetrics('promoClick', { 'type': metricLinkType, 'id': metricValue, 'url': elementToTrack.attr('href') });
        } else if (metricLinkType == "link") {
          console.log("********Lookbook Homepage :newLink Event triggered", metricValue);
          trackMetrics('newLink', { 'type': metricLinkType, 'id': metricValue });
        }
      } catch (excpt) {
        console.log("Lookbook metrics error:");
        console.log(excpt);
      };
    } else {
      console.log('trackMetrics has NOT beet called cause data-metrics-link-type param is not set to a link');
    }
    return true;
  }

  // target may be banner('.flex-molecule') or link('a')
  // for banners we take only section with molecule(not just one with background)
  // for links we take only those, which have '.flex2-molecule'
  impressionTrackingValue(target, props) {
    var valueArray = [],
      value = "",
      titleAttr, validRows;
    if (target.is('a')) {
      validRows = $(target.parents('.flex-content').find('.section:has(".flex2-molecule")')[0]);
    } else {
      validRows = $(target.find('.section:has(".flex2-molecule")')[0]);
    }
    // take 'data-metrics-title' value from molecule
    titleAttr = $(validRows.find('.flex2-molecule')[0]).attr('data-metrics-title');
    if (target.length === 0) return value;

    // gather properties from defaults
    for (var i = 0; i < props.length; i++) {
      var metricCnt = '';

      // gather 'data-metrics-position' attribute from '.flex-content'
      if (props[i].name == 'position') {
        if (target.length) {
          if (target.is('a')) {
            metricCnt = target.parents('.flex-content').attr("data-metrics-" + props[i].name);
          } else {
            metricCnt = target.attr("data-metrics-" + props[i].name);
          }
        } else {
          metricCnt = '';
        }
      }

      // handle 'data-metrics-title' value from molecule
      if (props[i].name == 'title') {
        if (target.length && (typeof titleAttr !== typeof undefined && titleAttr !== false)) {
          if (titleAttr.indexOf(' ') >= 0) {
            titleAttr = titleAttr.split(' ').join('-');
            metricCnt = titleAttr;
          } else {
            metricCnt = titleAttr;
          }

        } else {
          var tcmid = "banner" + "-" + $(validRows.find('.flex2-molecule')[0]).attr('data-metrics-tcmid');
          metricCnt = tcmid;
        }
      }
      if (metricCnt) {
        valueArray.push(metricCnt);
      }
    }
    if (valueArray.length) {
      value = valueArray.join('_');
    }
    return value;
  }

  // get link's identifier handle it and push to previously collected data
  linkTrackingValue(eleToTrack, props) {
    var trackingValue = this.impressionTrackingValue(eleToTrack, props),
      linkIdentifier = eleToTrack.attr("data-metrics-title");
    if (linkIdentifier && linkIdentifier.indexOf(' ') >= 0) {
      linkIdentifier = linkIdentifier.split(' ').join('-');
    }
    trackingValue = trackingValue + '_' + linkIdentifier;
    return trackingValue;
  }
}
