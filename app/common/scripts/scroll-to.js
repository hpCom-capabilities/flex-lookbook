'use strict';

export class ScrollTo {

  constructor(...args) {
    this.resolveAnchorsRows();
    this._initMobileScrolling();
    this.animating = false;
  }

  resolveAnchorsRows() {
    //editors can not put an anchor to a ROW. 
    $(() => {
      //if anchor is set via custom block with id -> set it to closest ROW
      $('.row-anchor-helper').each((index, el) => {
        let $el = $(el),
          id = $el.attr('id');

        if (id) {
          $el.closest('.section').attr('data-anchor', id);
          $el.remove();
        }
      });
      //if anchor is set via module data-anchor with no "anchor-to-module" classname
      // -> set it to closest ROW
      $('.flex2-molecule:not(.anchor-to-module)[data-anchor]').each((index, el) => {
        let $el = $(el),
          anchor = $el.attr('data-anchor');
          
        $el.removeAttr('data-anchor').closest('.section').attr('data-anchor', anchor);
      });
      $(window).trigger('anchors-resolved');
      window.GLOBALS.anchorsResolved = true;
      this.checkAnchorInUrl();
      this._initScrollToAnchor();
    });
  }

  /**
   * Any link on a page, CTA / regular - might be an anchor link.
   */
  _initScrollToAnchor() {
    $('.flex2-molecule:not(.molecule-lb-835):not(.molecule-lb-836) a[href^="#"]:not(custom-anchor):not(.scroll-to-next)').on('click', (e) => {
      let hash = $(e.currentTarget).attr('href').split('#')[1];
      $(`[data-anchor=${hash}]`).length && this.navigate(hash);
    });
  }

  _initMobileScrolling() {
    window.requestAnimFrame = (function(){
      return  window.requestAnimationFrame       ||
              window.webkitRequestAnimationFrame ||
              window.mozRequestAnimationFrame    ||
              function( callback ){
                window.setTimeout(callback, 1000 / 60);
              };
    })();
  }

  navigate(hash, cb, duration, el, offset) {
    let $el, 
        anchorEl, 
        top, 
        updatedNavHeight, 
        newTop,
        stickyNavHeight = $('.molecule-lb-835:not(.non-stickable)').outerHeight()
                          || $('.molecule-lb-830:not(.non-stickable)').outerHeight()
                          || $('.molecule-lb-836:not(.non-stickable)').outerHeight();
    offset = offset || 0;
    hash = el ? '' : hash.replace('#', '');
    anchorEl = el || $('div[data-anchor="' + hash + '"]');

    $el = anchorEl.length ? anchorEl : $('#' + hash);

    if ($el.length) {
      top = Math.floor($el.offset().top + 1);
      newTop = +top - +stickyNavHeight + offset;
      this._animate(newTop, duration, () => {
        anchorEl.trigger('activated-by-scroll-to');
        cb && cb();
      });
      return true;
    }
    return false;
  }

  navigateToOffset(offset, duration, cb, includeNavHeight) {
    let scrollTop = $(window).scrollTop(),
      newTop = scrollTop + offset;

    includeNavHeight && (newTop -= $('.molecule-lb-835').outerHeight());
    this._animate(newTop, duration, cb);
  }

  navigateToSection(hash, cb, duration, el, offset) {
    let $el,
      anchorEl,
      top,
      newTop;
    offset = offset || 0;
    hash = el ? '' : hash.replace('#', '');
    anchorEl = el || $('div[data-anchor="' + hash + '"]');

    $el = anchorEl.length ? anchorEl : $('#' + hash);

    if ($el.length) {
      top = Math.floor($el.offset().top + 1);
      newTop = +top + offset;
      this._animate(newTop, duration, () => {
        anchorEl.trigger('activated-by-scroll-to');
        cb && cb();
      });
      return true;
    }
    return false;
  }

  navigateTo(offset, duration, cb) {
    this._animate(offset, duration, cb);
  }

  _animate(newTop, duration = 600, cb) {
    this.animating = true;
    this._animateDefault(newTop, duration, 'easeInOutCubic', cb);
  }

  _animateDefault(newTop, duration, easing, cb) {
    this._scrollToMobile(newTop, duration, easing, function() {
      this.animating = false;
      cb && cb();
    }.bind(this));
  }

  _scrollToMobile(scrollToY, duration, easing, cb) {
    let scrollY = window.scrollY || window.pageYOffset,
        scrollTo = scrollToY || 0,
        currentTime = 0,
        time = duration / 1000,
        easingEquations = {
          easeInOutCubic: function(pos) {
            if ((pos/=0.5) < 1) return 0.5*Math.pow(pos,3);
            return 0.5 * (Math.pow((pos-2),3) + 2);
          }
        };

    easing = easing || 'easeInOutCubic';

    function tick() {
      currentTime += 1 / 60;

      let p = currentTime / time,
          t = easingEquations[easing](p);

      if (p < 1) {
        requestAnimFrame(tick);
        window.scrollTo(0, scrollY + ((scrollTo - scrollY) * t));
      } else {
        window.scrollTo(0, scrollTo);
        cb && cb();
      }
    }
    tick();
  }

  checkAnchorInUrl() {
    let urlAnchor = window.location.hash;
    if ( urlAnchor ) {
        urlAnchor = urlAnchor && urlAnchor.replace(/^#!/,'#');
        $(window).on('load', () => this.navigate( urlAnchor ));
    }
  }
}
