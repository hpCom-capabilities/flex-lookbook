'use strict';

export class Utils {

  constructor(...args) {
    this._init();
  }

  _init() {
    this.device = {
      isMobile: navigator.userAgent.indexOf('Windows NT') < 0 && ('ontouchstart' in window || navigator.msMaxTouchPoints || false),
      isLandscape: function() {
        return window.matchMedia("(orientation: landscape)").matches
      }
    }
    if (this.device.isMobile) {
      $(() => $('body').addClass('lb-mobile'));
    } else {
      $(() => $('body').addClass('lb-desktop'));
    }

    //Extend video options to have "isPlaying" param
    Object.defineProperty(HTMLMediaElement.prototype, 'isPlaying', {
      get: function(){
        return !!(this.currentTime > 0.01 && !this.paused && !this.ended && this.readyState > 2);
      }
    });
    this._initCache();
    if (this.isRTL()) {
      $(() => $('body').addClass('rtl-page'));
    }
  }

  _parseQuery(qstr) {
    let query = {},
      pairs = (qstr[0] === '?' ? qstr.substr(1) : qstr).split('&');

    for (let i = 0; i < pairs.length; i++) {
      let pair = pairs[i].split('=');
      query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
    }
    return query;
  }

  _initCache() {
    var cache = {};
    this.cache = {
      set: (name, value) => {
        cache[name] = [value];
      },
      push: (name, value) => {
        if (cache[name]) {
          cache[name].push(value);
        } else {
          this.cache.set(name, value);
        }
      },
      get: (name) => {
        return cache[name] || [];
      },
      remove: (name) => {
        delete cache[name]
      }
    }
  }

  getElTranslate(el) {
    let regExpr = new RegExp(/translate3d\((.*?)px/),
        style = el.attr('style'),
        test = regExpr.exec(style);

    if (test) return +test[1];

    return 0;
  }

  isRTL() {
    return $('html').attr('dir') === "rtl";
  }
}