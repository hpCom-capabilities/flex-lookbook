export let eventBus = {
  on(eventName, handler) {
    $(this).on(eventName, handler);
    return () => {
      this.off(eventName, handler);
    };
  },

  off(eventName, handler) {
    $(this).off(eventName, handler);
  },

  trigger(eventName, ...data) {
    $(this).triggerHandler(eventName, [...data]);
  }
};
