'use strict';

export class PageConfig {
  constructor() {
    this.data = {};

    //Adding global HP if it is not defined.
    (!window.HP) && (window.HP = {});

    //Adding pageConfig, cause on some pages there is inline usage of HP.pageConfig -> errors if not defined
    window.HP.pageConfig = this;
  }

  has(key) {
    return Object.prototype.hasOwnProperty.call(this.data, key);
  }

  set(key, value) {
    if (value === undefined || value === this.data[key])
      return false;

    if (this.has(key) && typeof this.get(key) === "object" && typeof value === "object") {
      for(let prop in value){
        this.data[key][prop] = value[prop];
      }
    } else {
      this.data[key] = value;
    }

    return this.data[key];
  }

  get(key) {
    return this.data[key];
  }

  drop(key) {
    key = key || null;

    if (key === null){
      delete this.data;
      this.data = {};
    } else if (this.has(key)) {
      delete this.data[key];
    }
  }
}