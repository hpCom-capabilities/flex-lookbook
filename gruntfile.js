const path = require('path');

const GRUNT_FOLDER = 'grunt';
const GLOBAL_CONFIG = {
  distFolder: 'dist',
  staticServerDir: '../flex-lookbook-static/lookbook/flex',
  staticFilesDir: '../flex-lookbook-static/lookbook/static',
  vendors: [
    'app/common/scripts/vendors/underscore-min.js',
    'app/common/scripts/vendors/slick.min.js',
    'app/common/scripts/vendors/jquery.viewportchecker.min.js',
    'app/common/scripts/vendors/stickyTableColumn.js',
    'app/common/scripts/vendors/jquery.mousewheel.js',
    'app/common/scripts/vendors/velocity.min.js'
  ]
};

module.exports = (grunt) => {
  grunt.config.merge(GLOBAL_CONFIG);

  require('load-grunt-config')(grunt, {
    configPath: path.join(__dirname, `${GRUNT_FOLDER}/config`),
    jitGrunt: {
      customTasksDir: `${GRUNT_FOLDER}/tasks`
    }
  });

  /**
   * updates local PATHs and
   *                     1. copies dist folder to ../flex-lookbook-static/lookbook/flex
   *                     2. copies app/common/static folder to ../flex-lookbook-static/lookbook/static
   *
   * NOTE, before run this method - make sure to run grunt serve, to build dist folder
   * once manually run this task please pull->commit->push 'flex-lookbook-static' to the server
   */
  grunt.registerTask('sync', [
    'clean:static',
    'copy:staticServer',
    'replace'
  ]);
};
