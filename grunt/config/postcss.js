module.exports = (grunt, options) => {
  return {
    options: {
      map: true,
      processors: [
        require('autoprefixer')({browsers: 'last 2 versions'})
      ]
    },
    dist: {
      src: `${grunt.config('distFolder')}/**/*.css`
    }
  };
};
