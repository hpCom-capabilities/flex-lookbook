module.exports = (grunt, options) => {
  const DIST = grunt.config('distFolder');

  return {
    dist: {
      options: {
        sourceMap: true
      },
      files: {
        [`./${DIST}/styles/lb-flex.css`]: 'app/lookbook.scss',
        [`./${DIST}/styles/dev.css`]: 'app/common/styles/dev.scss'
      }
    }
  };
};
