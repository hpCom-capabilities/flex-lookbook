module.exports = (grunt, options) => {
  const DEST = `./${grunt.config('distFolder')}/scripts/`;

  const MAIN_ENTRY_POINT = 'app/lookbook.js';
  const MAIN_BUNDLE = `${DEST}/lb-flex.js`;

  const transform = [
    'babelify', {
      'presets': [ 'es2015' ],
      'plugins': [ 'transform-class-properties' ]
    }
  ];

  return {
    development: {
      files: {
        [MAIN_BUNDLE]: MAIN_ENTRY_POINT
      },
      options: {
        watch: true,
        browserifyOptions: {
          debug: true
        },
        transform: [transform]
      }
    }
  };
};
