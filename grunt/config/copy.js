module.exports = (grunt, options) => {
  const DIST = grunt.config('distFolder');
  const staticServerDIST = grunt.config('staticServerDir');
  const staticFilesDIST = grunt.config('staticFilesDir');

  return {
    html: {
      files: [
        {
          expand: true,
          cwd: 'app',
          src: ['**/*.html', '!**/static/**'],
          dest: `${DIST}/`
        }
      ]
    },
    assets: {
      files: [
        {
          expand: true,
          flatten: true,
          src: ['app/common/fonts/hps/**'],
          dest: `${DIST}/fonts/hps`,
          filter: 'isFile'
        },
        {
          expand: true,
          flatten: true,
          src: ['app/common/fonts/icons/HPFlex2Icons/fonts/**'],
          dest: `${DIST}/fonts/icons`,
          filter: 'isFile'
        },
        {
          expand: true,
          src: ['**', '!lookbook/**'],
          cwd: 'app/common/images/',
          dest: `${DIST}/images`
        },
        {
          expand: true,
          src: ['lookbook/**/*'],
          cwd: 'app/common/images/',
          dest: `${DIST}/us/en/images/i`
        },
        {
          expand: true,
          flatten: true,
          src: ['app/common/scripts/vendors/jquery.min.js'],
          dest: `${DIST}/scripts`
        }
      ]
    },
    staticServer: {
      files: [
        {
          expand: true,
          cwd: `${DIST}/`,
          src: [`**`], 
          dest: `${staticServerDIST}`
        },
        {
          expand: true,
          cwd: 'app/common/static',
          src: [`**`], 
          dest: `${staticFilesDIST}`
        }
      ]
    }
  };
};
