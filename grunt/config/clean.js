module.exports = (grunt, options) => {
  const DIST = grunt.config('distFolder');
  const staticServerDIST = grunt.config('staticServerDir');
  const staticFilesDIST = grunt.config('staticFilesDir');

  return {
    options: {
      force: true
    },
    dist: [`${DIST}/**/*`],
    static: [`${staticServerDIST}/**/*`, `${staticFilesDIST}/**/*`]
  };
};
