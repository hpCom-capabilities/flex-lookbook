module.exports = (grunt, options) => {
  return {
    server: {
      options: {
        hostname: '*',
        port: 9005,
        base: grunt.config('distFolder'),
        open: true
      }
    }
  };
};
