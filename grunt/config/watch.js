module.exports = (grunt, options) => {
  return {

    styles: {
      files: 'app/**/*.scss',
      tasks: ['sass', 'postcss'],
      options: {
        event: ['all']
      }
    },

    html: {
      files: 'app/**/*.html',
      tasks: ['copy:html'],
      options: {
        event: ['all']
      }
    }
  };
};
