module.exports = (grunt, options) => {
  const DIST = grunt.config('distFolder');
  const VENDORS = grunt.config('vendors');

  return {
    options: {
      separator: ';'
    },
    vendors: {
      src: VENDORS,
      dest: `${DIST}/scripts/lb-vendors.js`
    },
    prod: {
      src: VENDORS.concat(`./${DIST}/scripts/build.js`),
      dest: `${DIST}/scripts/lookbook.js`
    }
  };
};
