module.exports = (grunt, options) => {
  const staticServerDIST = grunt.config('staticServerDir');

  return {
    dist: {
      options: {
        patterns: [
          {
            match: /([^a-z0-9])(\/styles)|([^a-z0-9])(\/scripts)|([^a-z0-9])(\/fonts)|([^a-z0-9])(\/images)/ig,
            replacement: function(a) {
              return a.replace(/([a-z]+)/ig, 'lookbook/flex/$1');
            }
          }
        ]
      },
      files: [
        {
          expand: true,
          cwd: `${staticServerDIST}/`,
          src: [`**/*.html`], 
          dest: `${staticServerDIST}`
        },
        {
          expand: true,
          cwd: `${staticServerDIST}/styles/`,
          src: [`**/*.css`], 
          dest: `${staticServerDIST}/styles`
        }
      ]
    }
  };
};